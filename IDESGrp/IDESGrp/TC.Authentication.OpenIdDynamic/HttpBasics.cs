﻿// HttpBasics.cs @ tom jones, because .NET core is missing some of the basic stuff

using System;
using System.Globalization;
using System.Text;

namespace TC.Authentication.OpenIdDynamic
{
    public class HttpBasics
    {
        /// <summary>
        ///  Decode values from a url for internal use
        /// </summary>
        public static string HttpDecode(string text)
        {
            StringBuilder sb = new StringBuilder(text.Length);
            int cText = text.Length;
            int cLen = 0;
            bool bArg = false;
            bool bNum = false;
            char cLast = (char)0;
            int result = 0;
            byte[] empty   = new byte[] { 0, 0, 0 };
            byte[] collect = new byte[] { 0, 0, 0 };
            for (int i = 0; i < cText; i++)
            {
                cLen = sb.Length;
                switch (text[i])
                {
                    case '&':
                        if (bArg)
                        { return sb.ToString(); }
                        bArg = true;
                        break;
                    case '#':
                        if (bArg)
                        {
                            bNum = true;
                            result = 0;
                        }
                        else
                        {
                            sb.Append(text[i]);
                        }
                        break;
                    case ';':
                        if (bNum)
                        {
                            sb.Append(result + '0');
                            bNum = false;
                            bArg = false;
                        }
                        if (bArg)
                        {
                            bArg = false;
                            sb.Append('?');
                        }
                        break;
                    default:
                        int cc = text[i];
                        if (bNum & text[i] >= '0' & text[i] <= '9')
                        {
                            // convert from the numeric code
                            result = result * 10 + cc;
                        }
                        else
                        {
                            sb.Append(text[i]);
                        }
                        break;
                }
            }
            return sb.ToString();
        }
        /// <summary>
        /// HTTP encodes a string for a URL
        /// </summary>
        /// <param name="text">The text string to encode. </param>
        /// <returns>The HTML-encoded text.</returns>
        public static string HttpEncode(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return null;

            StringBuilder sb = new StringBuilder(text.Length*2);

            bool bQuery = false;
            char cSpace = '_';
            char cLast = (char)0;
            int  iLast = 0;

            int cText = text.Length;
            for (int i = 0; i < cText; i++)
            {
                iLast = sb.Length;
                if (iLast > 0)
                    cLast = sb[iLast - 1];
                switch (text[i])
                {
                    case '<':
                        sb.Append("%3c");
                        break;
                    case '>':
                        sb.Append("%3e");
                        break;
                    case '?':
                        bQuery = true;
                        sb.Append('?');
                        break;
                    default:
                        int cc = text[i];
                        if (bQuery)
                        {
                            if (cc == 0x20)
                            {
                                if (cLast != ' ')
                                    sb.Append(cSpace);
                            }
                            else if (cc > 0x20)
                            {
                                sb.Append(text[i]);
                            }
                        }
                        else
                            sb.Append(text[i]);
                        break;
                }
            }
            return sb.ToString();
        }
    }
}
