// OpenIdDynamicOptions.cs Copyright (c) tom jones - derived work from .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using TC.Authentication.OpenIdDynamic;

namespace Microsoft.AspNetCore.Builder
{
 
    /// <summary>
    ///  Redirect behavior enum
    ///</summary>
    public enum OpenIdDynamicRedirectBehavior
    {
        /// <summary>
        /// Emits a 302 response to redirect the user agent to
        /// the OpenID Connect provider using a GET request.
        /// </summary>
        RedirectGet = 0,

        /// <summary>
        /// Emits an HTML form to redirect the user agent to
        /// the OpenID Connect provider using a POST request.
        /// </summary>
        FormPost = 1
    }
    /// <summary>
    /// Configuration options for <see cref="OpenIdDynamicMiddleware"/>
    /// </summary>
    public class OpenIdDynamicOptions : RemoteAuthenticationOptions
    {
#pragma warning disable 1591
        /// <summary>
        /// Initializes a new <see cref="OpenIdDynamicOptions"/>
        /// </summary>
        public OpenIdDynamicOptions()
            : this("OpenIdDynamic")                                  // The default Scheme name
        {
        }

        /// <summary>
        /// Initializes a new <see cref="OpenIdDynamicOptions"/>
        /// </summary>
        /// <remarks>
        /// Defaults:
        /// <para>AddNonceToRequest: true.</para>
        /// <para>BackchannelTimeout: 1 minute.</para>
        /// <para>ProtocolValidator: new <see cref="OpenIdConnectProtocolValidator"/>.</para>
        /// <para>RefreshOnIssuerKeyNotFound: true</para>
        /// <para>Scope: <see cref="OpenIdConnectScope.OpenIdProfile"/>.</para>
        /// <para>TokenValidationParameters: new <see cref="TokenValidationParameters"/> with AuthenticationScheme = authenticationScheme.</para>
        /// <para>UseTokenLifetime: false.</para>
        /// </remarks>
        /// <param name="authenticationScheme"> will be used to when creating the <see cref="System.Security.Claims.ClaimsIdentity"/> for the AuthenticationScheme property.</param>
        public OpenIdDynamicOptions(string authenticationScheme)
        {
            AuthenticationScheme = authenticationScheme;
            AutomaticChallenge = true;
            DisplayName = "OpenIdConnectDynamic";
            CallbackPath = new PathString("/signin-oidc");
            SignedOutCallbackPath = new PathString("/signout-callback-oidc");
            RemoteSignOutPath = new PathString("/signout-oidc");
            ProviderInformation = new Dictionary<string, OidProvider>();  // one entry for each registered provider
            Events = new OpenIdDynamicEvents();
            Scope.Add("openid");                                           // this scope is required for oidc
            Scope.Add("profile");                                          // if i cannot set these values to be optional - i may not be able to use them at all
            Scope.Add("email");                                            // TODO  both profile and email should not be here - add later IF and ONLY IF user not signed in

            ClaimActions.DeleteClaim("nonce");
            ClaimActions.DeleteClaim("aud");
            ClaimActions.DeleteClaim("azp");
            ClaimActions.DeleteClaim("acr");
            ClaimActions.DeleteClaim("amr");
            ClaimActions.DeleteClaim("iss");
            ClaimActions.DeleteClaim("iat");
            ClaimActions.DeleteClaim("nbf");
            ClaimActions.DeleteClaim("exp");
            ClaimActions.DeleteClaim("at_hash");
            ClaimActions.DeleteClaim("c_hash");
            ClaimActions.DeleteClaim("auth_time");
            ClaimActions.DeleteClaim("ipaddr");
            ClaimActions.DeleteClaim("platf");
            ClaimActions.DeleteClaim("ver");

            // http://openid.net/specs/openid-connect-core-1_0.html#StandardClaims
            ClaimActions.MapUniqueJsonKey("sub", "sub");
            ClaimActions.MapUniqueJsonKey("name", "name");
            ClaimActions.MapUniqueJsonKey("given_name", "given_name");
            ClaimActions.MapUniqueJsonKey("family_name", "family_name");
            ClaimActions.MapUniqueJsonKey("profile", "profile");
            ClaimActions.MapUniqueJsonKey("email", "email");

            ClaimActions.MapUniqueJsonKey("nickname", "nickname");   // mojeID provides this one as well as the above
        }

        /// <summary>
        /// Gets or sets the 'client_id', this is treated as a default value that should be overriden by a dynamic registration.
        /// </summary>
        public string ClientId { get; set; }

        ///<summary>
        ///Collection of configuration managers, registrations and other data per OP authority
        ///</summary>
        public Dictionary<string, OidProvider> ProviderInformation;

        /// <summary>
        /// Boolean to set whether the middleware should go to user info endpoint to retrieve additional claims or not after creating an identity from id_token received from token endpoint.
        /// The default is 'false'.
        /// </summary>
        public bool GetClaimsFromUserInfoEndpoint { get; set; }

        /// <summary>
        /// A collection of claim actions used to select values from the json user data and create Claims.
        /// </summary>
        public ClaimActionCollection ClaimActions { get; } = new ClaimActionCollection();

        /// <summary>
        /// Gets or sets if HTTPS is required for the metadata address or authority.
        /// The default is true. This should be disabled only in development environments.
        /// </summary>
        public bool RequireHttpsMetadata { get; set; } = true;

        /// <summary>
        /// Gets or sets the <see cref="OpenIdDynamicEvents"/> to notify when processing OpenIdConnect messages.
        /// </summary>
        public new OpenIdDynamicEvents Events
        {
            get { return (OpenIdDynamicEvents)base.Events; }
            set { base.Events = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="OpenIdConnectProtocolValidator"/> that is used to ensure that the 'id_token' received
        /// is valid per: http://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation
        /// </summary>
        /// <exception cref="ArgumentNullException">if 'value' is null.</exception>
        public OpenIdConnectProtocolValidator ProtocolValidator { get; set; } = new OpenIdConnectProtocolValidator()
        {
            RequireStateValidation = false,
            NonceLifetime = TimeSpan.FromMinutes(15)
        };

        /// <summary>
        /// The request path within the application's base path where the user agent will be returned after sign out from the identity provider.
        /// </summary>
        public PathString SignedOutCallbackPath { get; set; }

        /// <summary>
        /// The uri where the user agent will be returned to after application is signed out from the identity provider.
        /// The redirect will happen after the SignedOutCallbackPath is invoked.
        /// </summary>
        /// <remarks>This URI can be out of the application's domain. By default it points to the root.</remarks>
        public string PostLogoutRedirectUri { get; set; } = "/";

        /// <summary>
        /// Gets or sets if a metadata refresh should be attempted after a SecurityTokenSignatureKeyNotFoundException. This allows for automatic
        /// recovery in the event of a signature key rollover. This is enabled by default.
        /// </summary>
        public bool RefreshOnIssuerKeyNotFound { get; set; } = true;

        /// <summary>
        /// Gets or sets the method used to redirect the user agent to the identity provider.
        /// </summary>
        public OpenIdDynamicRedirectBehavior AuthenticationMethod { get; set; } = OpenIdDynamicRedirectBehavior.RedirectGet;

        /// <summary>
        /// Gets or sets the 'resource'.
        /// </summary>
        public string Resource { get; set; }

        /// <summary>
        /// Gets or sets the 'response_mode'.
        /// </summary>
        public string ResponseMode { get; set; } = "form_post";

        /// <summary>
        /// Gets the list of permissions to request.
        /// </summary>
        public ICollection<string> Scope { get; } = new HashSet<string>();

        /// <summary>
        /// Requests received on this path will cause the middleware to invoke SignOut using the SignInScheme.
        /// </summary>
        public PathString RemoteSignOutPath { get; set; }

        /// <summary>
        /// The Authentication Scheme to use with SignOut on the SignOutPath. SignInScheme will be used if this
        /// is not set.
        /// </summary>
        public string SignOutScheme { get; set; }

        /// <summary>
        /// Gets or sets the type used to secure data handled by the middleware.
        /// </summary>
        public ISecureDataFormat<AuthenticationProperties> StateDataFormat { get; set; }

        /// <summary>
        /// Gets or sets the type used to secure strings used by the middleware.
        /// </summary>
        public ISecureDataFormat<string> StringDataFormat { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="ISecurityTokenValidator"/> used to validate identity tokens.
        /// </summary>
        public ISecurityTokenValidator SecurityTokenValidator { get; set; } = new JwtSecurityTokenHandler();

        /// <summary>
        /// Gets or sets the parameters used to validate identity tokens.
        /// </summary>
        /// <remarks>Contains the types and definitions required for validating a token.</remarks>
        public TokenValidationParameters TokenValidationParameters { get; set; } = new TokenValidationParameters();

        /// <summary>
        /// Indicates that the authentication session lifetime (e.g. cookies) should match that of the authentication token.
        /// If the token does not provide lifetime information then normal session lifetimes will be used.
        /// This is disabled by default.
        /// </summary>
        public bool UseTokenLifetime { get; set; }

        /// <summary>
        /// Indicates if requests to the CallbackPath may also be for other components. If enabled the middleware will pass
        /// requests through that do not contain OpenIdConnect authentication responses. Disabling this and setting the
        /// CallbackPath to a dedicated endpoint may provide better error handling.
        /// This is disabled by default.
        /// </summary>
        public bool SkipUnrecognizedRequests { get; set; } = false;

        /// <summary>
        ///  Basic Authentication is only used for testing, this should be false in deployment
        /// </summary>
        public bool BasicAuthMethodRequied { get; set; } = false;

        /// <summary>
        /// list of base administrators by UserName
        /// </summary>
        public string[] BaseAdministrators { get; set; }
    }
}
