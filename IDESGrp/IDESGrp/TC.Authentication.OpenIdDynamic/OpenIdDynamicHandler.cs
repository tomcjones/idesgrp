// OpenIdDynamicHandler.cs Copyright (c) tom jones - derived work from .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Http.Features.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Protocols;   // ConnectionManager
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Diagnostics;

namespace TC.Authentication.OpenIdDynamic
{
    /// <summary>
    /// A per-request dynamic authentication handler for the OpenIdDynamicMiddleware.
    /// </summary>
    public class OpenIdDynamicHandler : RemoteAuthenticationHandler<OpenIdDynamicOptions>
    {
        private const string NonceProperty = "N";
        private const string UriSchemeDelimiter = "://";

        private const string InputTagFormat = @"<input type=""hidden"" name=""{0}"" value=""{1}"" />";
        private const string HtmlFormFormat = @"<!doctype html>
<html>
<head>
    <title>Please wait while you're being redirected to the identity provider</title>
</head>
<body>
    <form name=""form"" method=""post"" action=""{0}"">
        {1}
        <noscript>Click here to finish the process: <input type=""submit"" /></noscript>
    </form>
    <script>document.form.submit();</script>
</body>
</html>";

        private static readonly RandomNumberGenerator CryptoRandom = RandomNumberGenerator.Create();

        private OpenIdDynamicConfiguration _configuration;

        protected HttpClient Backchannel { get; private set; }

        protected HtmlEncoder HtmlEncoder { get; private set; }

        public OpenIdDynamicHandler(HttpClient backchannel, HtmlEncoder htmlEncoder)
        {
            HtmlEncoder = htmlEncoder;
        }

        public override async Task<bool> HandleRequestAsync()
        {
            if (Options.RemoteSignOutPath.HasValue && Options.RemoteSignOutPath == Request.Path)
            {
                return await HandleRemoteSignOutAsync();
            }
            else if (Options.SignedOutCallbackPath.HasValue && Options.SignedOutCallbackPath == Request.Path)
            {
                return await HandleSignOutCallbackAsync();
            }

            if (Options.CallbackPath != Request.Path)
            {
                return false;
            }
            // control reaches this point only if request is to CallbackPath, which means /signin-oidc for openID Connect
            bool bResult;
            try
            {
                bResult = await base.HandleRequestAsync();  // this code calls HandleRemoteAuthenticateAsync
            }
            catch (Exception ex)    //if not happy it throws an exception "Error from RemoteAuthentication"
            {
                string sMessage = ex.Message;          // TODO - convert to more user friendly messages
                int cCr = sMessage.IndexOf((char)13);  // is there a \r (carriage return) in the message?
                if (cCr > 0)
                {
                    sMessage = sMessage.Substring(0, cCr - 1);
                }                                      // TODO - If i can get here from manage i need to pick right callback
                string redirURI = HttpBasics.HttpEncode("/Account/ExternalLoginCallback?RemoteError=" + sMessage);
                Response.Redirect(redirURI);           // this is the response to the POST that redirects to login page
                return true;
            }
            return bResult;
        }

        protected virtual async Task<bool> HandleRemoteSignOutAsync()
        {
            OpenIdConnectMessage message = null;

            if (string.Equals(Request.Method, "GET", StringComparison.OrdinalIgnoreCase))
            {
                message = new OpenIdConnectMessage(Request.Query.Select(pair => new KeyValuePair<string, string[]>(pair.Key, pair.Value)));
            }

            // assumption: if the ContentType is "application/x-www-form-urlencoded" it should be safe to read as it is small.
            else if (string.Equals(Request.Method, "POST", StringComparison.OrdinalIgnoreCase)
              && !string.IsNullOrEmpty(Request.ContentType)
              // May have media/type; charset=utf-8, allow partial match.
              && Request.ContentType.StartsWith("application/x-www-form-urlencoded", StringComparison.OrdinalIgnoreCase)
              && Request.Body.CanRead)
            {
                var form = await Request.ReadFormAsync();
                message = new OpenIdConnectMessage(form.Select(pair => new KeyValuePair<string, string[]>(pair.Key, pair.Value)));
            }

            // TODO  QQQ how to know which provider is to be contacted?  -- or is it?
            var remoteSignOutContext = new RemoteSignOutContext(Context, Options, message);
            await Options.Events.RemoteSignOut(remoteSignOutContext);

            if (remoteSignOutContext.HandledResponse)
            {
                Logger.RemoteSignOutHandledResponse();
                return true;
            }
            if (remoteSignOutContext.Skipped)
            {
                Logger.RemoteSignOutSkipped();
                return false;
            }

            if (message == null)
            {
                return false;
            }

            // Try to extract the session identifier from the authentication ticket persisted by the sign-in handler.
            // If the identifier cannot be found, bypass the session identifier checks: this may indicate that the
            // authentication cookie was already cleared, that the session identifier was lost because of a lossy
            // external/application cookie conversion or that the identity provider doesn't support sessions.
            var sid = (await Context.Authentication.AuthenticateAsync(Options.SignOutScheme))
                          ?.FindFirst(JwtRegisteredClaimNames.Sid)
                          ?.Value;
            if (!string.IsNullOrEmpty(sid))
            {
                // Ensure a 'sid' parameter was sent by the identity provider.
                if (string.IsNullOrEmpty(message.Sid))
                {
                    Logger.RemoteSignOutSessionIdMissing();
                    return true;
                }
                // Ensure the 'sid' parameter corresponds to the 'sid' stored in the authentication ticket.
                if (!string.Equals(sid, message.Sid, StringComparison.Ordinal))
                {
                    Logger.RemoteSignOutSessionIdInvalid();
                    return true;
                }
            }

            Logger.RemoteSignOut();

            // We've received a remote sign-out request
            await Context.Authentication.SignOutAsync(Options.SignOutScheme);
            return true;
        }

        /// <summary>
        /// Redirect user to the identity provider for sign out
        /// </summary>
        /// <returns>A task executing the sign out procedure</returns>
        protected override async Task HandleSignOutAsync(SignOutContext signout)
        {
            Logger.EnteringOpenIdAuthenticationHandlerHandleSignOutAsync(GetType().FullName);
            /*
            if (_configuration == null && Options.ConfigurationManager != null)
            {
                _configuration = await Options.ConfigurationManager.GetConfigurationAsync(Context.RequestAborted);
            }
            */
            var message = new OpenIdConnectMessage()
            {
                IssuerAddress = _configuration?.EndSessionEndpoint ?? string.Empty,

                // Redirect back to SigneOutCallbackPath first before user agent is redirected to actual post logout redirect uri
                PostLogoutRedirectUri = BuildRedirectUriIfRelative(Options.SignedOutCallbackPath)
            };

            // Get the post redirect URI.
            var properties = new AuthenticationProperties(signout.Properties);
            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                properties.RedirectUri = BuildRedirectUriIfRelative(Options.PostLogoutRedirectUri);
                if (string.IsNullOrWhiteSpace(properties.RedirectUri))
                {
                    properties.RedirectUri = CurrentUri;
                }
            }
            Logger.PostSignOutRedirect(properties.RedirectUri);

            // Attach the identity token to the logout request when possible.
            message.IdTokenHint = await Context.Authentication.GetTokenAsync(Options.SignOutScheme, OpenIdConnectParameterNames.IdToken);

            var redirectContext = new RedirectContext(Context, Options, properties)
            {
                ProtocolMessage = message
            };

            await Options.Events.RedirectToIdentityProviderForSignOut(redirectContext);
            if (redirectContext.HandledResponse)
            {
                Logger.RedirectToIdentityProviderForSignOutHandledResponse();
                return;
            }
            else if (redirectContext.Skipped)
            {
                Logger.RedirectToIdentityProviderForSignOutSkipped();
                return;
            }

            message = redirectContext.ProtocolMessage;

            if (!string.IsNullOrEmpty(message.State))
            {
                properties.Items[OpenIdConnectDefaults.UserstatePropertiesKey] = message.State;
            }

            message.State = Options.StateDataFormat.Protect(properties);

            if (string.IsNullOrEmpty(message.IssuerAddress))
            {
                throw new InvalidOperationException(
                    "Cannot redirect to the end session endpoint, the configuration may be missing or invalid.");
            }

            if (Options.AuthenticationMethod == OpenIdDynamicRedirectBehavior.RedirectGet)
            {
                var redirectUri = message.CreateLogoutRequestUrl();
                if (!Uri.IsWellFormedUriString(redirectUri, UriKind.Absolute))
                {
                    Logger.InvalidLogoutQueryStringRedirectUrl(redirectUri);
                }

                Response.Redirect(redirectUri);
            }
            else if (Options.AuthenticationMethod == OpenIdDynamicRedirectBehavior.FormPost)
            {
                var inputs = new StringBuilder();
                foreach (var parameter in message.Parameters)
                {
                    var name = HtmlEncoder.Encode(parameter.Key);
                    var value = HtmlEncoder.Encode(parameter.Value);

                    var input = string.Format(CultureInfo.InvariantCulture, InputTagFormat, name, value);
                    inputs.AppendLine(input);
                }

                var issuer = HtmlEncoder.Encode(message.IssuerAddress);

                //Please wait while you are being redirected to the Identity Provider
                var content = string.Format(CultureInfo.InvariantCulture, HtmlFormFormat, issuer, inputs);
                var buffer = Encoding.UTF8.GetBytes(content);

                Response.ContentLength = buffer.Length;
                Response.ContentType = "text/html;charset=UTF-8";

                // Emit Cache-Control=no-cache to prevent client caching.
                Response.Headers[HeaderNames.CacheControl] = "no-cache";
                Response.Headers[HeaderNames.Pragma] = "no-cache";
                Response.Headers[HeaderNames.Expires] = "-1";

                await Response.Body.WriteAsync(buffer, 0, buffer.Length);
            }
            else
            {
                throw new NotImplementedException($"An unsupported authentication method has been configured: {Options.AuthenticationMethod}");
            }
        }

        /// <summary>
        /// Response to the callback from OpenId provider after session ended.
        /// </summary>
        /// <returns>A task executing the callback procedure</returns>
        protected virtual Task<bool> HandleSignOutCallbackAsync()
        {
            StringValues protectedState;
            if (Request.Query.TryGetValue(OpenIdConnectParameterNames.State, out protectedState))
            {
                var properties = Options.StateDataFormat.Unprotect(protectedState);
                if (!string.IsNullOrEmpty(properties?.RedirectUri))
                {
                    Response.Redirect(properties.RedirectUri);                    // do i care which provider is involved?
                    return Task.FromResult(true);
                }
            }

            return Task.FromResult(true);
        }

        /// <summary>
        /// Responds to a 401 Challenge. Sends an OpenIdConnect message to the 'identity authority' to obtain an identity.
        /// Arrives here from ChallengeAsync (when unauthorized) from ChallengeResult from Challenge
        /// </summary>
        /// <returns></returns>
        protected override async Task<bool> HandleUnauthorizedAsync(ChallengeContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            Logger.EnteringOpenIdAuthenticationHandlerHandleUnauthorizedAsync(GetType().FullName);

            // order for local RedirectUri
            // 1. challenge.Properties.RedirectUri
            // 2. CurrentUri if RedirectUri is not set)
            var properties = new AuthenticationProperties(context.Properties);

            if (string.IsNullOrEmpty(properties.RedirectUri))
            {
                properties.RedirectUri = CurrentUri;
            }
            Logger.PostAuthenticationLocalRedirect(properties.RedirectUri);
            string sEmail = "";
            bool bEmail= context.Properties.TryGetValue("XsrfId", out sEmail);
            if (!bEmail)
            {
                string redirURI = HttpBasics.HttpEncode("/Account/ExternalLoginCallback?RemoteError=Email_Address_not_supplied_" + sEmail);
                Response.Redirect(redirURI);            // this is the response to the POST that redirects to login page    QQQ   sometimes it should go to Manage.
                return true;
            }
            int cEmail = sEmail.IndexOf('@');
            OidProvider oProv = null;                    // this will be the provider that is associated with the email address from the user
            OidProvider nProv = null ;                   // only use if the issuer is not the same name as the Identity (email) provider

            ConfigurationManager<OpenIdDynamicConfiguration> oiConfig;   // populate from the ProviderConfiguration in Options, or build a new one

            Uri uIssuer = null;
            try                                                         // we need to have a provider configuration to proceed
            {
                string host = "";
                if (bEmail & (cEmail > 0))
                {
                    host = sEmail.Substring(cEmail+1).ToLower();
                    bool bProv = Options.ProviderInformation.TryGetValue(host, out oProv);
                
                    if (_configuration == null && bProv)
                    {
                        _configuration = await oProv.ProviderConfiguration.GetConfigurationAsync(Context.RequestAborted);
                    }
                }

                bool bSeparateIssuer = false;     // from identity provider name
                if (_configuration == null)
                {
                    char cLast = host.Last();
                    string sLast = host.Substring(host.Length - 1); 
                    var retr = new OpenIdDynamicConfigurationRetriever();
                    Backchannel = new HttpClient();
                    Backchannel.DefaultRequestHeaders.UserAgent.ParseAdd("TC OpenIdConnect Dynamic");
                    Backchannel.Timeout = Options.BackchannelTimeout;
                    Backchannel.MaxResponseContentBufferSize = 1024 * 1024 * 10; // 10 MB
                    var docr = new HttpDocumentRetriever(Backchannel) { RequireHttps = true };
                    // TODO - switch to the URI methods to catch any problems in the line below
                    oiConfig = new ConfigurationManager<OpenIdDynamicConfiguration>("https://" + host + "/.well-known/openid-configuration", retr, docr);  // TODO handle trailing "/"
                    _configuration = await oiConfig.GetConfigurationAsync(Context.RequestAborted);
                    if (_configuration == null) throw new Exception("Could not acquire a configuration");
                    // now we have a configuration to work from

                    oProv = new OidProvider("https://" + host, oiConfig, Backchannel);    // this ctor will create a retriever, amoung other things
                    Options.ProviderInformation.Add(host, oProv);
                    oProv.RegistrationStarted = DateTime.Now;      // used to determine when registration is successfully completed

                    // we need to see if there is already an issuer for this username and use any registration it might already have
                    // also if we do create an issuer record, it should have the same registration information

                    uIssuer = new Uri(_configuration.Issuer);
                    string sTestHost = uIssuer.IdnHost;
                    if (sTestHost != host)   // sometimes the issuer will be different from the host in the email addres - if so we need to capture both
                    {
                        bSeparateIssuer = true;
                        try
                        {
                            bool bProv = Options.ProviderInformation.TryGetValue(sTestHost, out nProv);
                            if (!bProv)
                            { 
                                nProv = new OidProvider("https://" + sTestHost, oiConfig, Backchannel);   // there is an assumption here that it is possible to get a configuration @ sTestHost
                                nProv.RegistrationCompleted = true;                // BUT - the configuration will come late  (TODO - need client ID and secret)
                                Options.ProviderInformation.Add(sTestHost, nProv);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.ExceptionProcessingMessage(ex);   // don't bother the user or the running system for this case (but the stack may fail later)
                        }
                    }
                }
                else
                {
                    Backchannel = oProv.Backchannel;
                }
            }
            catch  (Exception ex)         // no configuration means no oidc provider (OP) - tell the user we cannot contact the OP
            {
                Logger.UnableToAcquireConfiguration(sEmail + " " + ex.Message);
                string redirURI = HttpBasics.HttpEncode("/Account/ExternalLoginCallback?RemoteError=ID_Provider_not_found_for_"+sEmail);
                Response.Redirect(redirURI);           // this is the response to the POST that redirects to login page    QQQ   sometimes it should go to Manage.
                return true;
            }

            // ready to create registration request  --    TODO  also should check datetime to be sure that we don't still need one.
            if (!oProv.RegistrationCompleted)
            {
                var request = new OpenIdDynamicMessage
                {
                    ClientName = "IDESGrp",
                    RedirectUris = BuildRedirectUri(Options.CallbackPath),  // this returns a single string rather than an array
                    ResponseTypes = "code"                                  // "code id_token"  was failing because MSFT wont let me get to the #Fragment
                };
                string registrationReturn = await JsonExhangePost(request, _configuration.RegistrationEndpoint);
                oProv.ProviderRegistrationResponse = new OidRegistrationResponse(registrationReturn);
                string sError = oProv.ProviderRegistrationResponse.Error;
                if (!string.IsNullOrWhiteSpace(sError))
                {
                    Logger.UnableToAcquireRegistration(sEmail);
                    string redirURI = HttpBasics.HttpEncode("/Account/ExternalLoginCallback?RemoteError=ID_Provider_registration_error_" + sError);
                    Response.Redirect(redirURI);           // this is the response to the POST that redirects to login page
                    return true;
                }
                oProv.RegistrationCompleted = true;
            }
            // only "code" and "id_token" are valid response types for OpenID Connect, chose code when possible as ASP.NET will anot accept a #Fragment
            oProv.ResponseType = "id_token";    // this is the default when nothing is specified
            if (_configuration.ResponseTypesSupported.Contains("code"))
            { oProv.ResponseType = "code"; }
            if (oProv.ProviderRegistrationResponse.ResponseTypesSupported.Contains("code"))
            { oProv.ResponseType = "code"; }
            // ready to create message to bounce off the user's agent to the OP
            properties.Items.Add("DisplayName", oProv.AuthenticationScheme);      // usec to find the issuer name
            string sCId = Options.ClientId;  // only in case that there was none from the registration with the OP
            string sSecret = null;
            try
            {
                string cIdTest = oProv.ProviderRegistrationResponse.ClientId;     // use the Provider's Client ID
                if (!string.IsNullOrWhiteSpace(cIdTest)) { sCId = cIdTest; }      // if it exists
                sSecret = oProv.ProviderRegistrationResponse.ClientSecret;
            } finally { };                                                        // otherwise, stick with the default
            oProv.ClientID = sCId;                                                // save in the provider so i can find this provider again when i need it
            var message = new OpenIdConnectMessage
            {
                ClientId = sCId,
//                 ClientSecret = sSecret,                 // this would be used if method is post rather than basic
                IssuerAddress = _configuration?.AuthorizationEndpoint ?? string.Empty,
                RedirectUri = BuildRedirectUri(Options.CallbackPath),
                Resource = Options.Resource,
                ResponseType = oProv.ResponseType,        // this is a variable that i get from the registrations.  It should not be id_token for security
                Scope = string.Join(" ", Options.Scope)   // this just turns the HashSet into a space delimited string per openid spec
            };

            // Omitting the response_mode parameter when it already corresponds to the default
            // response_mode used for the specified response_type is recommended by the specifications.
            // See http://openid.net/specs/oauth-v2-multiple-response-types-1_0.html#ResponseModes
            if (!string.Equals(oProv.ResponseType, OpenIdConnectResponseType.Code, StringComparison.Ordinal) ||
                !string.Equals(Options.ResponseMode, OpenIdConnectResponseMode.Query, StringComparison.Ordinal))
            {
 //              message.ResponseMode = Options.ResponseMode;  // this caused mojeID to return server error 500 when used with id_token, changed default to code
            }

            if (Options.ProtocolValidator.RequireNonce)
            {
                message.Nonce = Options.ProtocolValidator.GenerateNonce();
                WriteNonceCookie(message.Nonce);
            }

            GenerateCorrelationId(properties);

            var redirectContext = new RedirectContext(Context, Options, properties)
            {
                ProtocolMessage = message
            };

            await Options.Events.RedirectToIdentityProvider(redirectContext);
            if (redirectContext.HandledResponse)
            {
                Logger.RedirectToIdentityProviderHandledResponse();
                return true;
            }
            else if (redirectContext.Skipped)
            {
                Logger.RedirectToIdentityProviderSkipped();
                return false;
            }
             
            message = redirectContext.ProtocolMessage;

            if (!string.IsNullOrEmpty(message.State))
            {
                properties.Items[OpenIdConnectDefaults.UserstatePropertiesKey] = message.State;
            }

            // When redeeming a 'code' for an AccessToken, this value is needed
            properties.Items.Add(OpenIdConnectDefaults.RedirectUriForCodePropertiesKey, message.RedirectUri);

            message.State = Options.StateDataFormat.Protect(properties);

            if (string.IsNullOrEmpty(message.IssuerAddress))
            {
                throw new InvalidOperationException(
                    "Cannot redirect to the authorization endpoint, the configuration may be missing or invalid.");
            }

            if (Options.AuthenticationMethod == OpenIdDynamicRedirectBehavior.RedirectGet)
            {
                var redirectUri = message.CreateAuthenticationRequestUrl();
                if (!Uri.IsWellFormedUriString(redirectUri, UriKind.Absolute))
                {
                    Logger.InvalidAuthenticationRequestUrl(redirectUri);
                }
                Response.Redirect(redirectUri);
                return true;
            }
            else if (Options.AuthenticationMethod == OpenIdDynamicRedirectBehavior.FormPost)
            {
                var inputs = new StringBuilder();
                foreach (var parameter in message.Parameters)
                {
                    var name = HtmlEncoder.Encode(parameter.Key);
                    var value = HtmlEncoder.Encode(parameter.Value);

                    var input = string.Format(CultureInfo.InvariantCulture, InputTagFormat, name, value);
                    inputs.AppendLine(input);
                }

                var issuer = HtmlEncoder.Encode(message.IssuerAddress);

                // Please wait while you are being redirected to the Identity Provider
                var content = string.Format(CultureInfo.InvariantCulture, HtmlFormFormat, issuer, inputs);
                var buffer = Encoding.UTF8.GetBytes(content);

                Response.ContentLength = buffer.Length;
                Response.ContentType = "text/html;charset=UTF-8";

                // Emit Cache-Control=no-cache to prevent client caching.
                Response.Headers[HeaderNames.CacheControl] = "no-cache";
                Response.Headers[HeaderNames.Pragma] = "no-cache";
                Response.Headers[HeaderNames.Expires] = "-1";

                await Response.Body.WriteAsync(buffer, 0, buffer.Length);
                return true;
            }

            throw new NotImplementedException($"An unsupported authentication method has been configured: {Options.AuthenticationMethod}");
        }

        /// <summary>
        /// Invoked to process incoming OpenIdConnect messages to /signin-oidc.
        /// </summary>
        /// <returns>An <see cref="AuthenticationTicket"/> if successful.</returns>
        protected override async Task<AuthenticateResult> HandleRemoteAuthenticateAsync()
        {
            Logger.EnteringOpenIdAuthenticationHandlerHandleRemoteAuthenticateAsync(GetType().FullName);

            OpenIdConnectMessage authorizationResponse = null;

            // ASP.NET CORE has eliminated the ability to get the raw URL and does not support fragments, so some OpenID Connect implicit feature are not supported

            if (string.Equals(Request.Method, "GET", StringComparison.OrdinalIgnoreCase))
            {
                authorizationResponse = new OpenIdConnectMessage(Request.Query.Select(pair => new KeyValuePair<string, string[]>(pair.Key, pair.Value)));

                // response_mode=query (explicit or not) and a response_type containing id_token
                // or token are not considered as a safe combination and MUST be rejected.
                // See http://openid.net/specs/oauth-v2-multiple-response-types-1_0.html#Security
                if (!string.IsNullOrEmpty(authorizationResponse.IdToken) || !string.IsNullOrEmpty(authorizationResponse.AccessToken))
                {
                    if (Options.SkipUnrecognizedRequests)
                    {
                        // Not for us?
                        return AuthenticateResult.Skip();
                    }
                    return AuthenticateResult.Fail("An OpenID Connect response cannot contain an " +
                            "identity token or an access token when using response_mode=query");
                }
            }
            // assumption: if the ContentType is "application/x-www-form-urlencoded" it should be safe to read as it is small.
            else if (string.Equals(Request.Method, "POST", StringComparison.OrdinalIgnoreCase)
              && !string.IsNullOrEmpty(Request.ContentType)
              // May have media/type; charset=utf-8, allow partial match.
              && Request.ContentType.StartsWith("application/x-www-form-urlencoded", StringComparison.OrdinalIgnoreCase)
              && Request.Body.CanRead)
            {
                var form = await Request.ReadFormAsync();
                authorizationResponse = new OpenIdConnectMessage(form.Select(pair => new KeyValuePair<string, string[]>(pair.Key, pair.Value)));
            }

            OidProvider oProv = null;
            string sCid = authorizationResponse.ClientId;
            string sIss = authorizationResponse.Iss;
            Uri uIssuer = new Uri(authorizationResponse.Iss);
            string sHost = uIssuer.AbsoluteUri;
            bool bProv = Options.ProviderInformation.TryGetValue(sHost, out oProv);
            if (bProv)
            {
                Backchannel = oProv.Backchannel;
            }
            else
            {
                foreach (KeyValuePair<string, OidProvider> entry in Options.ProviderInformation)
                {
                    string baseAddress = entry.Value.BaseAddress;
                    if (!string.IsNullOrWhiteSpace(baseAddress))
                    {
                        if (sIss.StartsWith(baseAddress))
                        {
                            oProv = entry.Value;
                            Backchannel = oProv.Backchannel;
                        }
                    }
                }
            }

            if (Backchannel == null)
            {
                throw (new Exception("Could not find the provider"));   // TODO handle exception better
            }

            if (authorizationResponse == null)
            {
                if (Options.SkipUnrecognizedRequests)
                {
                    // Not for us?
                    return AuthenticateResult.Skip();
                }
                return AuthenticateResult.Fail("No message.");
            }

            AuthenticateResult result;
            string sCId = Options.ClientId;  // only in case that there was none from the registration with the OP
            string sSecret = null;

            try
            {
                AuthenticationProperties properties = null;
                if (!string.IsNullOrEmpty(authorizationResponse.State))
                {
                    properties = Options.StateDataFormat.Unprotect(authorizationResponse.State);
                }

                var messageReceivedContext = await RunMessageReceivedEventAsync(authorizationResponse, properties);
                if (messageReceivedContext.CheckEventResult(out result))
                {
                    return result;
                }
                authorizationResponse = messageReceivedContext.ProtocolMessage;
                properties = messageReceivedContext.Properties;

                if (properties == null)
                {
                    // Fail if state is missing, it's required for the correlation id.
                    if (string.IsNullOrEmpty(authorizationResponse.State))
                    {
                        // This wasn't a valid OIDC message, it may not have been intended for us.
                        Logger.NullOrEmptyAuthorizationResponseState();
                        if (Options.SkipUnrecognizedRequests)
                        {
                            return AuthenticateResult.Skip();
                        }
                        return AuthenticateResult.Fail(Resources.MessageStateIsNullOrEmpty);
                    }

                    // if state exists and we failed to 'unprotect' this is not a message we should process.
                    properties = Options.StateDataFormat.Unprotect(authorizationResponse.State);
                }

                if (properties == null)
                {
                    Logger.UnableToReadAuthorizationResponseState();
                    if (Options.SkipUnrecognizedRequests)
                    {
                        // Not for us?
                        return AuthenticateResult.Skip();
                    }
                    return AuthenticateResult.Fail(Resources.MessageStateIsInvalid);
                }

                string userstate = null;
                properties.Items.TryGetValue(OpenIdConnectDefaults.UserstatePropertiesKey, out userstate);
                authorizationResponse.State = userstate;

                if (!ValidateCorrelationId(properties))
                {
                    return AuthenticateResult.Fail("Correlation failed.");
                }

                // if any of the error fields are set, throw error null
                if (!string.IsNullOrEmpty(authorizationResponse.Error))
                {
                    return AuthenticateResult.Fail(CreateOpenIdConnectProtocolException(authorizationResponse, response: null));
                }

                if (_configuration == null)       // ensure we have the configuration for this provider
                {
                    Logger.UpdatingConfiguration();
                    _configuration = await oProv.ProviderConfiguration.GetConfigurationAsync(Context.RequestAborted);
                }
                try
                {
                    string cIdTest = oProv.ProviderRegistrationResponse.ClientId;  // use the Provider's Client ID
                    if (!string.IsNullOrWhiteSpace(cIdTest)) { sCId = cIdTest; }      // if it exists
                    sSecret = oProv.ProviderRegistrationResponse.ClientSecret;
                }
                finally { };

                if (_configuration == null)     // if we still don't have the configuration - give up
                {
                    throw new OpenIdConnectProtocolException($"Faild to find configuration in HandleRemoteAuthenticateAsync");
                }

                PopulateSessionProperties(authorizationResponse, properties);

                AuthenticationTicket ticket = null;
                JwtSecurityToken jwt = null;
                string nonce = null;
                Options.TokenValidationParameters.ValidAudience = sCId;     // this action assures that the value of this field is worthless anywhere else
                var validationParameters = Options.TokenValidationParameters.Clone();   // now the validAudience is dynamic for use in this close ONLY

                // Hybrid or Implicit flow
                if (!string.IsNullOrEmpty(authorizationResponse.IdToken))
                {
                    Logger.ReceivedIdToken();
                    ticket = ValidateToken(authorizationResponse.IdToken, oProv.AuthenticationScheme, properties, validationParameters, out jwt);

                    nonce = jwt.Payload.Nonce;
                    if (!string.IsNullOrEmpty(nonce))
                    {
                        nonce = ReadNonceCookie(nonce);
                    }

                    var tokenValidatedContext = await RunTokenValidatedEventAsync(authorizationResponse, null, properties, ticket, jwt, nonce);
                    if (tokenValidatedContext.CheckEventResult(out result))
                    {
                        return result;
                    }
                    authorizationResponse = tokenValidatedContext.ProtocolMessage;
                    properties = tokenValidatedContext.Properties;
                    ticket = tokenValidatedContext.Ticket;
                    jwt = tokenValidatedContext.SecurityToken;
                    nonce = tokenValidatedContext.Nonce;
                }

                // there is only one validator for all providers, but the validation context seems to be per connection - is that safe?
                Options.ProtocolValidator.ValidateAuthenticationResponse(new OpenIdConnectProtocolValidationContext()
                {
                    ClientId = sCId,
                    ProtocolMessage = authorizationResponse,
                    ValidatedIdToken = jwt,
                    Nonce = nonce
                });

                OpenIdConnectMessage tokenEndpointResponse = null;

                // Authorization Code or Hybrid flow
                if (!string.IsNullOrEmpty(authorizationResponse.Code))
                {
                    authorizationResponse.ClientSecret = sSecret;  // needed for HTTP Authorization credential - the only one that the Token Endpoint MUST support
                    var authorizationCodeReceivedContext = await RunAuthorizationCodeReceivedEventAsync(authorizationResponse, properties, ticket, jwt);
                    if (authorizationCodeReceivedContext.CheckEventResult(out result))
                    {
                        return result;
                    }
                    authorizationResponse = authorizationCodeReceivedContext.ProtocolMessage;
                    properties = authorizationCodeReceivedContext.Properties;
                    var tokenEndpointRequest = authorizationCodeReceivedContext.TokenEndpointRequest;
                    // If the developer redeemed the code themselves...
                    tokenEndpointResponse = authorizationCodeReceivedContext.TokenEndpointResponse;
                    ticket = authorizationCodeReceivedContext.Ticket;
                    jwt = authorizationCodeReceivedContext.JwtSecurityToken;

                    if (!authorizationCodeReceivedContext.HandledCodeRedemption) 
                    {
                        ICollection<string> sTokenEndpointAuthMethodsSupported = _configuration.TokenEndpointAuthMethodsSupported;
                        bool bToke_post = sTokenEndpointAuthMethodsSupported.Contains("client_secret_post");
                        if (bToke_post)
                        {
                            if (string.IsNullOrEmpty(tokenEndpointRequest.ClientSecret))  // to exchange code for id_token we need to provide our secret
                            {
                                tokenEndpointRequest.ClientSecret = sSecret;
                            }
                        }
                        else
                        {

                        }
                        tokenEndpointResponse = await RedeemAuthorizationCodeAsync(tokenEndpointRequest);
                        // check message to be sure that there were no errors
                    }

                    var tokenResponseReceivedContext = await RunTokenResponseReceivedEventAsync(authorizationResponse, tokenEndpointResponse, properties, ticket);
                    if (tokenResponseReceivedContext.CheckEventResult(out result))
                    {
                        return result;
                    }

                    authorizationResponse = tokenResponseReceivedContext.ProtocolMessage;
                    tokenEndpointResponse = tokenResponseReceivedContext.TokenEndpointResponse;

                    string sError = tokenEndpointResponse.Error;
                    if (!string.IsNullOrWhiteSpace(sError))
                    {
                        if (!string.IsNullOrWhiteSpace(tokenEndpointResponse.ErrorDescription))
                        {
                            sError = sError + " " + tokenEndpointResponse.ErrorDescription;
                        }
                        Exception ex = new Exception("Error " + sError);
                        return AuthenticateResult.Fail(ex);
                    }

                    // no need to validate signature when token is received using "code flow" as per spec
                    // [http://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation].
                    validationParameters.RequireSignedTokens = false;

                    // At least a cursory validation is required on the new IdToken, even if we've already validated the one from the authorization response.
                    // And we'll want to validate the new JWT in ValidateTokenResponse.
                    JwtSecurityToken tokenEndpointJwt = null;
                    AuthenticationTicket tokenEndpointTicket = null;
                    try
                    {
                        tokenEndpointTicket = ValidateToken(tokenEndpointResponse.IdToken, oProv.AuthenticationScheme, properties, validationParameters, out tokenEndpointJwt);
                    }
                    catch (Exception ex)  // this probably means we have an error recorded in the tokenresponseReceivedContext
                    {
                        return AuthenticateResult.Fail(ex);
                    }
                    // Avoid reading & deleting the nonce cookie, running the event, etc, if it was already done as part of the authorization response validation.
                    if (ticket == null)
                    {
                        nonce = tokenEndpointJwt.Payload.Nonce;
                        if (!string.IsNullOrEmpty(nonce))
                        {
                            nonce = ReadNonceCookie(nonce);
                        }

                        var tokenValidatedContext = await RunTokenValidatedEventAsync(authorizationResponse, tokenEndpointResponse, properties, tokenEndpointTicket, tokenEndpointJwt, nonce);
                        if (tokenValidatedContext.CheckEventResult(out result))
                        {
                            return result;
                        }
                        authorizationResponse = tokenValidatedContext.ProtocolMessage;
                        tokenEndpointResponse = tokenValidatedContext.TokenEndpointResponse;
                        properties = tokenValidatedContext.Properties;
                        ticket = tokenValidatedContext.Ticket;
                        jwt = tokenValidatedContext.SecurityToken;
                        nonce = tokenValidatedContext.Nonce;
                    }
                    else
                    {
                        if (!string.Equals(jwt.Subject, tokenEndpointJwt.Subject, StringComparison.Ordinal))
                        {
                            throw new SecurityTokenException("The sub claim does not match in the id_token's from the authorization and token endpoints.");
                        }

                        jwt = tokenEndpointJwt;
                    }

                    // Validate the token response if it wasn't provided manually
                    if (!authorizationCodeReceivedContext.HandledCodeRedemption)
                    {
                        Options.ProtocolValidator.ValidateTokenResponse(new OpenIdConnectProtocolValidationContext()
                        {
                            ClientId = sCId,
                            ProtocolMessage = tokenEndpointResponse,
                            ValidatedIdToken = jwt,
                            Nonce = nonce
                        });
                    }
                }

                if (Options.SaveTokens)
                {
                    SaveTokens(ticket.Properties, tokenEndpointResponse ?? authorizationResponse);
                }

                if (Options.GetClaimsFromUserInfoEndpoint)
                {
                    return await GetUserInformationAsync(tokenEndpointResponse ?? authorizationResponse, jwt, ticket);
                }
                else
                {
                    var identity = (ClaimsIdentity)ticket.Principal.Identity;
                    foreach (var action in Options.ClaimActions)
                    {
                        action.Run(null, identity, Options.ClaimsIssuer);
                    }
                }

                return AuthenticateResult.Success(ticket);
            }
            catch (Exception exception)
            {
                Logger.ExceptionProcessingMessage(exception);
                /*
                // Refresh the configuration for exceptions that may be caused by key rollovers. The user can also request a refresh in the event.
                if (Options.RefreshOnIssuerKeyNotFound && exception is SecurityTokenSignatureKeyNotFoundException)
                {
                    if (Options.ConfigurationManager != null)  //  <<  this is no longer part of Options, but oidProvider
                    {
                        Logger.ConfigurationManagerRequestRefreshCalled();
                        Options.ConfigurationManager.RequestRefresh();
                    }
                }
                */
                var authenticationFailedContext = await RunAuthenticationFailedEventAsync(authorizationResponse, exception);
                if (authenticationFailedContext.CheckEventResult(out result))
                {
                    return result;
                }

                return AuthenticateResult.Fail(exception);
            }
        }

        private void PopulateSessionProperties(OpenIdConnectMessage message, AuthenticationProperties properties)
        {
            if (!string.IsNullOrEmpty(message.SessionState))
            {
                properties.Items[OpenIdConnectSessionProperties.SessionState] = message.SessionState;
            }

            if (!string.IsNullOrEmpty(_configuration.CheckSessionIframe))
            {
                properties.Items[OpenIdConnectSessionProperties.CheckSessionIFrame] = _configuration.CheckSessionIframe;
            }
        }

        /// <summary>
        /// Redeems the authorization code for tokens at the token endpoint.
        /// </summary>
        /// <param name="tokenEndpointRequest">The request that will be sent to the token endpoint and is available for customization.</param>
        /// <param name="sAuthorization">If not null this string will be added as part of a Authorization Header</param>
        /// <returns>OpenIdConnect message that has tokens inside it.</returns>
        protected virtual async Task<OpenIdConnectMessage> RedeemAuthorizationCodeAsync(OpenIdConnectMessage tokenEndpointRequest, string sAuthorization = null)
        {
            Logger.RedeemingCodeForTokens();

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, _configuration.TokenEndpoint);
            requestMessage.Content = new FormUrlEncodedContent(tokenEndpointRequest.Parameters);
            if (!String.IsNullOrEmpty(sAuthorization))
            {
                requestMessage.Headers.Add("Authorization", sAuthorization);
            }
            var responseMessage = await Backchannel.SendAsync(requestMessage);     // go redeem AuthZ Code

            var contentMediaType = responseMessage.Content.Headers.ContentType?.MediaType;
            int iStatusCode = (int)responseMessage.StatusCode;
            if (string.IsNullOrEmpty(contentMediaType))
            {
                Logger.LogDebug($"Unexpected token response format. Status Code: {iStatusCode}. Content-Type header is missing.");
            }
            else if (!string.Equals(contentMediaType, "application/json", StringComparison.OrdinalIgnoreCase))
            {
                Logger.LogDebug($"Unexpected token response format. Status Code: {iStatusCode}. Content-Type {responseMessage.Content.Headers.ContentType}.");
            }

            // Error handling:
            // 1. If the response body can't be parsed as json, throws.
            // 2. If the response's status code is not in 2XX range, throw OpenIdConnectProtocolException. If the body is correct parsed,
            //    pass the error information from body to the exception.
            OpenIdConnectMessage message;

            var request = responseMessage.Headers.WwwAuthenticate;
             
            if (!responseMessage.IsSuccessStatusCode)
            {
                string sError = string.Format("number {0}, {1}", iStatusCode, responseMessage.ReasonPhrase);
                string sX = "{\"error\": \"" + sError + ". Could not parse response from Token Endpoint\"}";  // Default error for user
                string sResponse = responseMessage.Content.ReadAsStringAsync().Result;   // but first let's see if there is a json message in body
                string sBody = sResponse.Substring(sResponse.IndexOf("<body>"));
                int iStart = sBody.IndexOf('{');
                int iEnd = sBody.IndexOf('}');
                if (iEnd > iStart)
                {
                    sX = sBody.Substring(iStart, iEnd - iStart + 1);
                }
                Logger.LogDebug($"Unexpected token response {sX}. Status Code: {iStatusCode}. Content-Type {responseMessage.Content.Headers.ContentType}.");
                message = new OpenIdConnectMessage(sX);
                return message;
            }
            try
            {
                var responseContent = await responseMessage.Content.ReadAsStringAsync();
                message = new OpenIdConnectMessage(responseContent);
            }
            catch (Exception ex)
            {
                throw new OpenIdConnectProtocolException($"Failed to parse token response body as JSON. Status Code: {(int)responseMessage.StatusCode}. Content-Type: {responseMessage.Content.Headers.ContentType}", ex);
            }

            return message;
        }
        /// <summary>
        /// Redeems a json POST request for a json response at any endpoint.
        /// </summary>
        /// <param name="endpointRequest">The request that will be sent to the specified endpoint.</param>
        /// <returns>OpenIdConnect message that has tokens inside it.</returns>
        protected virtual async Task<string> JsonExhangePost(OpenIdDynamicMessage endpointRequest, string endpoint)
        {
            Logger.RedeemingCodeForTokens();

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, endpoint);
            string json = JsonConvert.SerializeObject(endpointRequest.Parameters);
            HttpContent encoded = new FormUrlEncodedContent(endpointRequest.Parameters);
            requestMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");

            var responseMessage = await Backchannel.SendAsync(requestMessage);      // go get one Json object in exchange for another

            var contentMediaType = responseMessage.Content.Headers.ContentType?.MediaType;
            if (string.IsNullOrEmpty(contentMediaType))
            {
                Logger.LogDebug($"Unexpected token response format. Status Code: {(int)responseMessage.StatusCode}. Content-Type header is missing.");
            }
            else if (!string.Equals(contentMediaType, "application/json", StringComparison.OrdinalIgnoreCase))
            {
                Logger.LogDebug($"Unexpected token response format. Status Code: {(int)responseMessage.StatusCode}. Content-Type {responseMessage.Content.Headers.ContentType}.");
            }

            // Error handling:
            // 1. If the response body can't be parsed as json, throws.
            // 2. If the response's status code is not in 2XX range, throw OpenIdConnectProtocolException. If the body is correct parsed,
            //    pass the error information from body to the exception.
            OpenIdDynamicMessage dmessage;
            OpenIdConnectMessage cmessage;   // TODO convert exception code to dynamic
            string responseContent;
            try
            {
                responseContent = await responseMessage.Content.ReadAsStringAsync();

                cmessage = new OpenIdConnectMessage(responseContent);
                dmessage = new OpenIdDynamicMessage(responseContent);
            }
            catch (Exception ex)
            {
                throw new OpenIdConnectProtocolException($"Failed to parse token response body as JSON. Status Code: {(int)responseMessage.StatusCode}. Content-Type: {responseMessage.Content.Headers.ContentType}", ex);
            }

            if (!responseMessage.IsSuccessStatusCode)
            {

                throw CreateOpenIdConnectProtocolException(cmessage, responseMessage);
            }

            return responseContent;
        }
        /// <summary>
        /// 
        /// </summary>
        private async Task<RegistrationReturnedContext> RunRegistrationReturnedEventAsync(OpenIdDynamicMessage registrationRequest)
        {
            Logger.RegistrationReturned();

            var registrationResponseContext = new RegistrationReturnedContext(Context, Options)
        /// 
        /// </summary>
        /// <param name="registrationReturned"></param>
        /// <param name="properties"></param>
        /// <param name="ticket"></param>
        /// <param name="jwt"></param>
        /// <returns></returns>
            {
                ProtocolMessage = registrationRequest,
            };

            await Options.Events.RegistrationReturned(registrationResponseContext);
            if (registrationResponseContext.HandledResponse)
            {
                Logger.TokenValidatedHandledResponse();  // TODO fixup
            }
            else if (registrationResponseContext.Skipped)
            {
                Logger.TokenValidatedSkipped();  //  TODO  fixup
            }

            return registrationResponseContext;
        }

        /// <summary>
        /// Goes to UserInfo endpoint to retrieve additional claims and add any unique claims to the given identity.
        /// </summary>
        /// <param name="message">message that is being processed</param>
        /// <param name="jwt">The <see cref="JwtSecurityToken"/>.</param>
        /// <param name="ticket">authentication ticket with claims principal and identities</param>
        /// <returns>Authentication ticket with identity with additional claims, if any.</returns>
        protected virtual async Task<AuthenticateResult> GetUserInformationAsync(OpenIdConnectMessage message, JwtSecurityToken jwt, AuthenticationTicket ticket)
        {
            var userInfoEndpoint = _configuration?.UserInfoEndpoint;

            if (string.IsNullOrEmpty(userInfoEndpoint))
            {
                Logger.UserInfoEndpointNotSet();
                return AuthenticateResult.Success(ticket);
            }
            if (string.IsNullOrEmpty(message.AccessToken))
            {
                Logger.AccessTokenNotAvailable();
                return AuthenticateResult.Success(ticket);
            }
            Logger.RetrievingClaims();
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, userInfoEndpoint);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", message.AccessToken);
            var responseMessage = await Backchannel.SendAsync(requestMessage);     // go get user information
            responseMessage.EnsureSuccessStatusCode();
            var userInfoResponse = await responseMessage.Content.ReadAsStringAsync();

            JObject user;
            var contentType = responseMessage.Content.Headers.ContentType;
            if (contentType.MediaType.Equals("application/json", StringComparison.OrdinalIgnoreCase))
            {
                user = JObject.Parse(userInfoResponse);
            }
            else if (contentType.MediaType.Equals("application/jwt", StringComparison.OrdinalIgnoreCase))
            {
                var userInfoEndpointJwt = new JwtSecurityToken(userInfoResponse);
                user = JObject.FromObject(userInfoEndpointJwt.Payload);
            }
            else
            {
                return AuthenticateResult.Fail("Unknown response type: " + contentType.MediaType);
            }

            var userInformationReceivedContext = await RunUserInformationReceivedEventAsync(ticket, message, user);
            AuthenticateResult result;
            if (userInformationReceivedContext.CheckEventResult(out result))
            {
                return result;
            }
            ticket = userInformationReceivedContext.Ticket;
            user = userInformationReceivedContext.User;

            Options.ProtocolValidator.ValidateUserInfoResponse(new OpenIdConnectProtocolValidationContext()
            {
                UserInfoEndpointResponse = userInfoResponse,
                ValidatedIdToken = jwt,
            });

            var identity = (ClaimsIdentity)ticket.Principal.Identity;

            foreach (var action in Options.ClaimActions)
            {
                action.Run(user, identity, Options.ClaimsIssuer);
            }

            return AuthenticateResult.Success(ticket);
        }

        /// <summary>
        /// Save the tokens contained in the <see cref="OpenIdConnectMessage"/> in the <see cref="ClaimsPrincipal"/>.
        /// </summary>
        /// <param name="properties">The <see cref="AuthenticationProperties"/> in which tokens are saved.</param>
        /// <param name="message">The OpenID Connect response.</param>
        private void SaveTokens(AuthenticationProperties properties, OpenIdConnectMessage message)
        {
            var tokens = new List<AuthenticationToken>();

            if (!string.IsNullOrEmpty(message.AccessToken))
            {
                tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.AccessToken, Value = message.AccessToken });
            }

            if (!string.IsNullOrEmpty(message.IdToken))
            {
                tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.IdToken, Value = message.IdToken });
            }

            if (!string.IsNullOrEmpty(message.RefreshToken))
            {
                tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.RefreshToken, Value = message.RefreshToken });
            }

            if (!string.IsNullOrEmpty(message.TokenType))
            {
                tokens.Add(new AuthenticationToken { Name = OpenIdConnectParameterNames.TokenType, Value = message.TokenType });
            }

            if (!string.IsNullOrEmpty(message.ExpiresIn))
            {
                int value;
                if (int.TryParse(message.ExpiresIn, NumberStyles.Integer, CultureInfo.InvariantCulture, out value))
                {
                    var expiresAt = Options.SystemClock.UtcNow + TimeSpan.FromSeconds(value);
                    // https://www.w3.org/TR/xmlschema-2/#dateTime
                    // https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx
                    tokens.Add(new AuthenticationToken { Name = "expires_at", Value = expiresAt.ToString("o", CultureInfo.InvariantCulture) });
                }
            }

            properties.StoreTokens(tokens);
        }

        /// <summary>
        /// Adds the nonce to <see cref="HttpResponse.Cookies"/>.
        /// </summary>
        /// <param name="nonce">the nonce to remember.</param>
        /// <remarks><see cref="M:IResponseCookies.Append"/> of <see cref="HttpResponse.Cookies"/> is called to add a cookie with the name: 'OpenIdConnectAuthenticationDefaults.Nonce + <see cref="M:ISecureDataFormat{TData}.Protect"/>(nonce)' of <see cref="OpenIdDynamicOptions.StringDataFormat"/>.
        /// The value of the cookie is: "N".</remarks>
        private void WriteNonceCookie(string nonce)
        {
            if (string.IsNullOrEmpty(nonce))
            {
                throw new ArgumentNullException(nameof(nonce));
            }

            Response.Cookies.Append(
                OpenIdConnectDefaults.CookieNoncePrefix + Options.StringDataFormat.Protect(nonce),
                NonceProperty,
                new CookieOptions
                {
                    HttpOnly = true,
                    Secure = Request.IsHttps,
                    Expires = Options.SystemClock.UtcNow.Add(Options.ProtocolValidator.NonceLifetime)
                });
        }

        /// <summary>
        /// Searches <see cref="HttpRequest.Cookies"/> for a matching nonce.
        /// </summary>
        /// <param name="nonce">the nonce that we are looking for.</param>
        /// <returns>echos 'nonce' if a cookie is found that matches, null otherwise.</returns>
        /// <remarks>Examine <see cref="IRequestCookieCollection.Keys"/> of <see cref="HttpRequest.Cookies"/> that start with the prefix: 'OpenIdConnectAuthenticationDefaults.Nonce'.
        /// <see cref="M:ISecureDataFormat{TData}.Unprotect"/> of <see cref="OpenIdDynamicOptions.StringDataFormat"/> is used to obtain the actual 'nonce'. If the nonce is found, then <see cref="M:IResponseCookies.Delete"/> of <see cref="HttpResponse.Cookies"/> is called.</remarks>
        private string ReadNonceCookie(string nonce)
        {
            if (nonce == null)
            {
                return null;
            }

            foreach (var nonceKey in Request.Cookies.Keys)
            {
                if (nonceKey.StartsWith(OpenIdConnectDefaults.CookieNoncePrefix))
                {
                    try
                    {
                        var nonceDecodedValue = Options.StringDataFormat.Unprotect(nonceKey.Substring(OpenIdConnectDefaults.CookieNoncePrefix.Length, nonceKey.Length - OpenIdConnectDefaults.CookieNoncePrefix.Length));
                        if (nonceDecodedValue == nonce)
                        {
                            var cookieOptions = new CookieOptions
                            {
                                HttpOnly = true,
                                Secure = Request.IsHttps
                            };

                            Response.Cookies.Delete(nonceKey, cookieOptions);
                            return nonce;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.UnableToProtectNonceCookie(ex);
                    }
                }
            }

            return null;
        }

        private AuthenticationProperties GetPropertiesFromState(string state)
        {
            // assume a well formed query string: <a=b&>OpenIdConnectAuthenticationDefaults.AuthenticationPropertiesKey=kasjd;fljasldkjflksdj<&c=d>
            var startIndex = 0;
            if (string.IsNullOrEmpty(state) || (startIndex = state.IndexOf(OpenIdConnectDefaults.AuthenticationPropertiesKey, StringComparison.Ordinal)) == -1)
            {
                return null;
            }

            var authenticationIndex = startIndex + OpenIdConnectDefaults.AuthenticationPropertiesKey.Length;
            if (authenticationIndex == -1 || authenticationIndex == state.Length || state[authenticationIndex] != '=')
            {
                return null;
            }

            // scan rest of string looking for '&'
            authenticationIndex++;
            var endIndex = state.Substring(authenticationIndex, state.Length - authenticationIndex).IndexOf("&", StringComparison.Ordinal);

            // -1 => no other parameters are after the AuthenticationPropertiesKey
            if (endIndex == -1)
            {
                return Options.StateDataFormat.Unprotect(Uri.UnescapeDataString(state.Substring(authenticationIndex).Replace('+', ' ')));
            }
            else
            {
                return Options.StateDataFormat.Unprotect(Uri.UnescapeDataString(state.Substring(authenticationIndex, endIndex).Replace('+', ' ')));
            }
        }

        private async Task<MessageReceivedContext> RunMessageReceivedEventAsync(OpenIdConnectMessage message, AuthenticationProperties properties)
        {
            Logger.MessageReceived(message.BuildRedirectUrl());
            var messageReceivedContext = new MessageReceivedContext(Context, Options)
            {
                ProtocolMessage = message,
                Properties = properties,
            };

            await Options.Events.MessageReceived(messageReceivedContext);
            if (messageReceivedContext.HandledResponse)
            {
                Logger.MessageReceivedContextHandledResponse();
            }
            else if (messageReceivedContext.Skipped)
            {
                Logger.MessageReceivedContextSkipped();
            }

            return messageReceivedContext;
        }

        private async Task<TokenValidatedContext> RunTokenValidatedEventAsync(OpenIdConnectMessage authorizationResponse, OpenIdConnectMessage tokenEndpointResponse, AuthenticationProperties properties, AuthenticationTicket ticket, JwtSecurityToken jwt, string nonce)
        {
            var tokenValidatedContext = new TokenValidatedContext(Context, Options)
            {
                ProtocolMessage = authorizationResponse,
                TokenEndpointResponse = tokenEndpointResponse,
                Properties = properties,
                Ticket = ticket,
                SecurityToken = jwt,
                Nonce = nonce,
            };

            await Options.Events.TokenValidated(tokenValidatedContext);
            if (tokenValidatedContext.HandledResponse)
            {
                Logger.TokenValidatedHandledResponse();
            }
            else if (tokenValidatedContext.Skipped)
            {
                Logger.TokenValidatedSkipped();
            }

            return tokenValidatedContext;
        }

        private async Task<AuthorizationCodeReceivedContext> RunAuthorizationCodeReceivedEventAsync(OpenIdConnectMessage authorizationResponse, AuthenticationProperties properties, AuthenticationTicket ticket, JwtSecurityToken jwt)
        {
            Logger.AuthorizationCodeReceived();

            var tokenEndpointRequest = new OpenIdConnectMessage()
            {
                ClientId = authorizationResponse.ClientId,
                ClientSecret = authorizationResponse.ClientSecret,
                Code = authorizationResponse.Code,
                GrantType = OpenIdConnectGrantTypes.AuthorizationCode,
                RedirectUri = properties.Items[OpenIdConnectDefaults.RedirectUriForCodePropertiesKey]
            };

            string sAuthz = authorizationResponse.ClientId + ":" + authorizationResponse.ClientSecret;
            string sCred = "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(sAuthz));
            var headers = Backchannel.DefaultRequestHeaders;
            string azHeader = headers.Authorization?.ToString();
            bool bazHeader = true;  // true means to add Authorization header to list of defaults
            if (!string.IsNullOrWhiteSpace(azHeader))
            {
                bazHeader = azHeader != sCred;  // true means the existing header needs replaced
                if (bazHeader)
                {
                    Backchannel.DefaultRequestHeaders.Remove("Authorization");
                }
            }
            if (bazHeader)
            { 
            Backchannel.DefaultRequestHeaders.Add("Authorization", sCred);   // QQQ this should be only to the current oProv????
            }
            HttpRequestHeaders brh = Backchannel.DefaultRequestHeaders;
            bool bAuthHeader = brh.Contains("Authorization");
            IEnumerable<string>  sxcred = null;
            bool bCred = brh.TryGetValues("Authorization", out sxcred);

            var authorizationCodeReceivedContext = new AuthorizationCodeReceivedContext(Context, Options)
            {
                ProtocolMessage = authorizationResponse,
                Properties = properties,
                TokenEndpointRequest = tokenEndpointRequest,
                Ticket = ticket,
                JwtSecurityToken = jwt,
                Backchannel = Backchannel,
            };

            await Options.Events.AuthorizationCodeReceived(authorizationCodeReceivedContext);
            if (authorizationCodeReceivedContext.HandledResponse)
            {
                Logger.AuthorizationCodeReceivedContextHandledResponse();
            }
            else if (authorizationCodeReceivedContext.Skipped)
            {
                Logger.AuthorizationCodeReceivedContextSkipped();
            }

            return authorizationCodeReceivedContext;
        }

        private async Task<TokenResponseReceivedContext> RunTokenResponseReceivedEventAsync(
            OpenIdConnectMessage message,
            OpenIdConnectMessage tokenEndpointResponse,
            AuthenticationProperties properties,
            AuthenticationTicket ticket)
        {
            Logger.TokenResponseReceived();
            var eventContext = new TokenResponseReceivedContext(Context, Options, properties)
            {
                ProtocolMessage = message,
                TokenEndpointResponse = tokenEndpointResponse,
                Ticket = ticket
            };

            await Options.Events.TokenResponseReceived(eventContext);
            if (eventContext.HandledResponse)
            {
                Logger.TokenResponseReceivedHandledResponse();
            }
            else if (eventContext.Skipped)
            {
                Logger.TokenResponseReceivedSkipped();
            }

            return eventContext;
        }

        private async Task<UserInformationReceivedContext> RunUserInformationReceivedEventAsync(AuthenticationTicket ticket, OpenIdConnectMessage message, JObject user)
        {
            Logger.UserInformationReceived(user.ToString());

            var userInformationReceivedContext = new UserInformationReceivedContext(Context, Options)
            {
                Ticket = ticket,
                ProtocolMessage = message,
                User = user,
            };

            await Options.Events.UserInformationReceived(userInformationReceivedContext);
            if (userInformationReceivedContext.HandledResponse)
            {
                Logger.UserInformationReceivedHandledResponse();
            }
            else if (userInformationReceivedContext.Skipped)
            {
                Logger.UserInformationReceivedSkipped();
            }

            return userInformationReceivedContext;
        }

        private async Task<AuthenticationFailedContext> RunAuthenticationFailedEventAsync(OpenIdConnectMessage message, Exception exception)
        {
            var authenticationFailedContext = new AuthenticationFailedContext(Context, Options)
            {
                ProtocolMessage = message,
                Exception = exception
            };

            await Options.Events.AuthenticationFailed(authenticationFailedContext);
            if (authenticationFailedContext.HandledResponse)
            {
                Logger.AuthenticationFailedContextHandledResponse();
            }
            else if (authenticationFailedContext.Skipped)
            {
                Logger.AuthenticationFailedContextSkipped();
            }

            return authenticationFailedContext;
        }

        private AuthenticationTicket ValidateToken(string idToken, string authenticationScheme, AuthenticationProperties properties, TokenValidationParameters validationParameters, out JwtSecurityToken jwt)
        {
            if (!Options.SecurityTokenValidator.CanReadToken(idToken))
            {
                Logger.UnableToReadIdToken(idToken);
                throw new SecurityTokenException(string.Format(CultureInfo.InvariantCulture, Resources.UnableToValidateToken, idToken));
            }

            if (_configuration != null)
            {
                if (string.IsNullOrEmpty(validationParameters.ValidIssuer))
                {
                    validationParameters.ValidIssuer = _configuration.Issuer;
                }
                else if (!string.IsNullOrEmpty(_configuration.Issuer))
                {
                    validationParameters.ValidIssuers = validationParameters.ValidIssuers?.Concat(new[] { _configuration.Issuer }) ?? new[] { _configuration.Issuer };
                }

                validationParameters.IssuerSigningKeys = validationParameters.IssuerSigningKeys?.Concat(_configuration.SigningKeys) ?? _configuration.SigningKeys;
            }

            SecurityToken validatedToken = null;
            var principal = Options.SecurityTokenValidator.ValidateToken(idToken, validationParameters, out validatedToken);
            jwt = validatedToken as JwtSecurityToken;
            if (jwt == null)
            {
                Logger.InvalidSecurityTokenType(validatedToken?.GetType().ToString());
                throw new SecurityTokenException(string.Format(CultureInfo.InvariantCulture, Resources.ValidatedSecurityTokenNotJwt, validatedToken?.GetType()));
            }

            if (validatedToken == null)
            {
                Logger.UnableToValidateIdToken(idToken);
                throw new SecurityTokenException(string.Format(CultureInfo.InvariantCulture, Resources.UnableToValidateToken, idToken));
            }
            
            var ticket = new AuthenticationTicket(principal, properties,  authenticationScheme);

            if (Options.UseTokenLifetime)
            {
                var issued = validatedToken.ValidFrom;
                if (issued != DateTime.MinValue)
                {
                    ticket.Properties.IssuedUtc = issued;
                }

                var expires = validatedToken.ValidTo;
                if (expires != DateTime.MinValue)
                {
                    ticket.Properties.ExpiresUtc = expires;
                }
            }

            return ticket;
        }

        /// <summary>
        /// Build a redirect path if the given path is a relative path.
        /// </summary>
        private string BuildRedirectUriIfRelative(string uri)
        {
            if (string.IsNullOrEmpty(uri))
            {
                return uri;
            }

            if (!uri.StartsWith("/", StringComparison.Ordinal))
            {
                return uri;
            }

            return BuildRedirectUri(uri);
        }

        private OpenIdConnectProtocolException CreateOpenIdConnectProtocolException(OpenIdConnectMessage message, HttpResponseMessage response)
        {
            var description = message.ErrorDescription ?? "error_description is null";
            var errorUri = message.ErrorUri ?? "error_uri is null";

            if (response != null)
            {
                Logger.ResponseErrorWithStatusCode(message.Error, description, errorUri, (int)response.StatusCode);
            }
            else
            {
                Logger.ResponseError(message.Error, description, errorUri);
            }

            return new OpenIdConnectProtocolException(string.Format(
                CultureInfo.InvariantCulture,
                Resources.MessageContainsError,
                message.Error,
                description,
                errorUri));
        }
    }
}
