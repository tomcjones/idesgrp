﻿// OpenIdDynameMiddleware.cs Copyright (c) tom jones - derived work from .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.


using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using Microsoft.AspNetCore.Authentication;

namespace TC.Authentication.OpenIdDynamic
{
    /// <summary>
    /// Custome middleware for obtaining identities using a Dynamic OpenIdConnect protocol.
    /// The overriden method contains a method Invoke which creates and calls a handler
    ///  var handler = CreateHandler();
    ///  await handler.InitializeAsync (Options, httpContext, Logger, UrlEncoder)
    ///  try
    ///  await handler.HandleRequestAsync();
    ///  await next only if the handler returns false
    /// </summary>
    public class OpenIdDynamicMiddleware : AuthenticationMiddleware<OpenIdDynamicOptions>
    {
        /// <summary>
        /// Initializes a <see cref="OpenIdDynamicMiddleware"/>
        /// </summary>
        /// <param name="next">The next middleware in the middleware pipeline to invoke.</param>
        /// <param name="dataProtectionProvider"> provider for creating a data protector.</param>
        /// <param name="loggerFactory">factory for creating a <see cref="ILogger"/>.</param>
        /// <param name="encoder"></param>
        /// <param name="services"></param>
        /// <param name="sharedOptions"></param>
        /// <param name="optIdDynamic"></param>
        /// <param name="htmlEncoder">The <see cref="HtmlEncoder"/>.</param>
        public OpenIdDynamicMiddleware(
            RequestDelegate next,
            IDataProtectionProvider dataProtectionProvider,
            ILoggerFactory loggerFactory,
            UrlEncoder encoder,
            IServiceProvider services,
            IOptions<SharedAuthenticationOptions> sharedOptions,
            IOptions<OpenIdDynamicOptions> optIdDynamic,
            HtmlEncoder htmlEncoder)
            : base(next, optIdDynamic, loggerFactory, encoder)
        {
            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }

            if (dataProtectionProvider == null)
            {
                throw new ArgumentNullException(nameof(dataProtectionProvider));
            }

            if (loggerFactory == null)
            {
                throw new ArgumentNullException(nameof(loggerFactory));
            }

            if (encoder == null)
            {
                throw new ArgumentNullException(nameof(encoder));
            }

            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if (sharedOptions == null)
            {
                throw new ArgumentNullException(nameof(sharedOptions));
            }

            if (optIdDynamic == null)
            {
                throw new ArgumentNullException(nameof(optIdDynamic));
            }

            if (htmlEncoder == null)
            {
                throw new ArgumentNullException(nameof(htmlEncoder));
            }

            if (string.IsNullOrEmpty(Options.ClientId))
            {
                throw new ArgumentException("Options.ClientId must be provided", nameof(Options.ClientId));
            }

            if (!Options.CallbackPath.HasValue)
            {
                throw new ArgumentException("Options.CallbackPath must be provided.");
            }

            if (string.IsNullOrEmpty(Options.SignInScheme))
            {
                Options.SignInScheme = sharedOptions.Value.SignInScheme;
            }
            if (string.IsNullOrEmpty(Options.SignInScheme))
            {
                throw new ArgumentException("Options.SignInScheme is required.");
            }
            if (string.IsNullOrEmpty(Options.SignOutScheme))
            {
                Options.SignOutScheme = Options.SignInScheme;
            }

            HtmlEncoder = htmlEncoder;

            if (Options.StateDataFormat == null)
            {
                var dataProtector = dataProtectionProvider.CreateProtector(
                    typeof(OpenIdDynamicMiddleware).FullName,
                    typeof(string).FullName,
                    Options.AuthenticationScheme,
                    "v1");

                Options.StateDataFormat = new PropertiesDataFormat(dataProtector);
            }

            if (Options.StringDataFormat == null)
            {
                var dataProtector = dataProtectionProvider.CreateProtector(
                    typeof(OpenIdDynamicMiddleware).FullName,
                    typeof(string).FullName,
                    Options.AuthenticationScheme,
                    "v1");

                Options.StringDataFormat = new SecureDataFormat<string>(new StringSerializer(), dataProtector);
            }

            if (Options.Events == null)
            {
                Options.Events = new OpenIdDynamicEvents();
            }

            if (string.IsNullOrEmpty(Options.TokenValidationParameters.ValidAudience) && !string.IsNullOrEmpty(Options.ClientId))
            {
                Options.TokenValidationParameters.ValidAudience = Options.ClientId;   // TODO  make this dynamic qqq now or perhaps later?  = or should it always be dynamic?
            }

            // add these to the ctor for ProviderInformation
            Backchannel = new HttpClient( new HttpClientHandler() { ClientCertificateOptions = ClientCertificateOption.Manual });
            Backchannel.DefaultRequestHeaders.UserAgent.ParseAdd("TC OpenIdConnect Dynamic"); 
            Backchannel.Timeout = Options.BackchannelTimeout;
            Backchannel.MaxResponseContentBufferSize = 1024 * 1024 * 10; // 10 MB
            
            Options.ProviderInformation.Add("agaton-sax.com", new OidProvider("http://agaton-sax.com:8050", Backchannel));
            Options.ProviderInformation.Add("openid0.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-response_type-code", Backchannel));
            Options.ProviderInformation.Add("openid1.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-token_endpoint-client_secret_basic", Backchannel));
            Options.ProviderInformation.Add("openid2.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-nonce-invalid", Backchannel));
            Options.ProviderInformation.Add("openid3.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-scope-userinfo-claims/", Backchannel));
            Options.ProviderInformation.Add("openid4.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-aud", Backchannel));
            Options.ProviderInformation.Add("openid5.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-kid-absent-single-jwks", Backchannel));
            Options.ProviderInformation.Add("openid6.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-sig-none", Backchannel));
            Options.ProviderInformation.Add("openid7.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-issuer-mismatch", Backchannel));
            Options.ProviderInformation.Add("openid8.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-kid-absent-multiple-jwks", Backchannel));
            Options.ProviderInformation.Add("openid9.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-bad-sig-rs256/", Backchannel));
            Options.ProviderInformation.Add("openida.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-iat/", Backchannel));
            Options.ProviderInformation.Add("openidb.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-sig-rs256/", Backchannel));
            Options.ProviderInformation.Add("openidc.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-id_token-sub", Backchannel));
            Options.ProviderInformation.Add("openide.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-userinfo-bad-sub-claim", Backchannel));
            Options.ProviderInformation.Add("openidf.net", new OidProvider("https://rp.certification.openid.net:8080/IDESGrp0/rp-userinfo-bearer-header", Backchannel));
        }

        protected HttpClient Backchannel { get; private set; }

        protected HtmlEncoder HtmlEncoder { get; private set; }

        /// <summary>
        /// Called by UseOpenIdDynamic
        /// </summary>
        /// <returns></returns>
        protected override AuthenticationHandler<OpenIdDynamicOptions> CreateHandler()
        {
            return new OpenIdDynamicHandler(Backchannel, HtmlEncoder);
        }

        private class StringSerializer : IDataSerializer<string>
        {
            public string Deserialize(byte[] data)
            {
                return Encoding.UTF8.GetString(data);
            }

            public byte[] Serialize(string model)
            {
                return Encoding.UTF8.GetBytes(model);
            }
        }
    }
}
