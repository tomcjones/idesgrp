﻿// OpenIdDynmamicAppBuilderExt.cs Copyright (c) tom jones - derived work from .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Protocols;
using TC.Authentication.OpenIdDynamic;


namespace Microsoft.AspNetCore.Builder
{
    /// Override optIdDynamic to include a section per host site configuration to cache configurations

    /// <summary>
    /// Extension methods to add OpenID Dynamic authentication capabilities to an HTTP application pipeline.
    /// </summary>
    public static class OpenIdDynamicAppBuilderExtensions
    {
        /// <summary>
        /// Adds middleware which enables Dynameic OpenID Connect authentication capabilities.
        /// </summary>
        /// <param name="app">The <see cref="IApplicationBuilder"/> to add the middleware to.</param>
        /// <param name="options">An inestance of <see cref="OpenIdDynamicOptions"/> that specifies options for the middleware.</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IApplicationBuilder UseOpenIdDynamicAuthentication(this IApplicationBuilder app, OpenIdDynamicOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            return app.UseMiddleware<OpenIdDynamicMiddleware>(Options.Create(options));
        }
    }
}
