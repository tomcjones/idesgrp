// OpenIdDynamicMessage copyright @ tom jones

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Reflection;
using Microsoft.IdentityModel.Protocols;

namespace TC.Authentication.OpenIdDynamic
{
    /// <summary>
    /// Parameter names for OpenIdConnect.
    /// </summary>
    public static class OpenIdParameterNames
    {
#pragma warning disable 1591
        public const string AccessToken = "access_token";
        public const string AcrValues = "acr_values";
        public const string ClaimsLocales = "claims_locales";
        public const string ClientAssertion = "client_assertion";
        public const string ClientAssertionType = "client_assertion_type";
        public const string ClientId = "client_id";
        public const string ClientName = "client_name";
        public const string ClientSecret = "client_secret";
        public const string Code = "code";
        public const string Display = "display";
        public const string DomainHint = "domain_hint";
        public const string Error = "error";
        public const string ErrorDescription = "error_description";
        public const string ErrorUri = "error_uri";
        public const string ExpiresIn = "expires_in";
        public const string GrantType = "grant_type";
        public const string Iss = "iss";
        public const string IdToken = "id_token";
        public const string IdTokenHint = "id_token_hint";
        public const string IdentityProvider = "identity_provider";
        public const string LoginHint = "login_hint";
        public const string MaxAge = "max_age";
        public const string Nonce = "nonce";
        public const string Password = "password";
        public const string PostLogoutRedirectUri = "post_logout_redirect_uri";
        public const string Prompt = "prompt";
        public const string RedirectUri = "redirect_uri";
        public const string RedirectUris = "redirect_uris";
        public const string RefreshToken = "refresh_token";
        public const string RequestUri = "request_uri";
        public const string Resource = "resource";
        public const string ResponseMode = "response_mode";
        public const string ResponseType = "response_type";
        public const string ResponseTypes = "response_types";
        public const string Scope = "scope";
        public const string SkuTelemetry = "x-client-SKU";
        public const string SessionState = "session_state";
        public const string Sid = "sid";
        public const string State = "state";
        public const string TargetLinkUri = "target_link_uri";
        public const string TokenType = "token_type";
        public const string UiLocales = "ui_locales";
        public const string UserId = "user_id";
        public const string Username = "username";
        public const string VersionTelemetry = "x-client-ver";

    }
    //
    // Summary:
    //     RequestTypes for OpenIdConnect.
    //
    // Remarks:
    //     Can be used to determine the message type by consumers of an Microsoft.IdentityModel.Protocols.OpenIdConnect.OpenIdConnectMessage.
    //     For example: Microsoft.IdentityModel.Protocols.OpenIdConnect.OpenIdConnectMessage.CreateAuthenticationRequestUrl
    //     sets Microsoft.IdentityModel.Protocols.OpenIdConnect.OpenIdConnectMessage.RequestType
    //     to Microsoft.IdentityModel.Protocols.OpenIdConnect.OpenIdConnectRequestType.Authentication.
    public enum OpenIdDynamicRequestType
    {
        //
        // Summary:
        //     Indicates an Authentication Request see: http://openid.net/specs/openid-connect-core-1_0.html#AuthRequest.
        Authentication = 0,
        //
        // Summary:
        //     Indicates a Logout Request see:http://openid.net/specs/openid-connect-frontchannel-1_0.html#RPLogout.
        Logout = 1,
        //
        // Summary:
        //     Indicates a Token Request see: http://openid.net/specs/openid-connect-core-1_0.html#TokenRequest.
        Token = 2
    }
    /// <summary>
    /// Provides access to common OpenIdConnect parameters.
    /// </summary>
    public class OpenIdDynamicMessage : AuthenticationProtocolMessage
    {
        private const string _skuTelemetryValue = "ID_NET";

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenIdDynamicMessage"/> class.
        /// </summary>
        public OpenIdDynamicMessage() { }

        /// <summary>
        /// Initializes an instance of <see cref="OpenIdDynamicMessage"/> class with a json string.
        /// </summary>
        public OpenIdDynamicMessage(string json)
        {
            if (string.IsNullOrEmpty(json))
                throw LogHelper.LogArgumentNullException("json");

            try
            {
                SetJsonParameters(JObject.Parse(json));
            }
            catch
            {
                throw LogHelper.LogExceptionMessage(new ArgumentException(String.Format(CultureInfo.InvariantCulture, "IDX10106: Error in deserializing to json: '{0}'", json)));
            }

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenIdDynamicMessage"/> class.
        /// </summary>
        /// <param name="other"> an <see cref="OpenIdDynamicMessage"/> to copy.</param>
        /// <exception cref="ArgumentNullException">If 'other' is null.</exception>
        protected OpenIdDynamicMessage(OpenIdDynamicMessage other)
        {
            if (other == null)
                throw LogHelper.LogArgumentNullException("other");

            foreach (KeyValuePair<string, string> keyValue in other.Parameters)
            {
                SetParameter(keyValue.Key, keyValue.Value);
            }

            AuthorizationEndpoint = other.AuthorizationEndpoint;
            IssuerAddress = other.IssuerAddress;
            RequestType = other.RequestType;
            TokenEndpoint = other.TokenEndpoint;
            EnableTelemetryParameters = other.EnableTelemetryParameters;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenIdDynamicMessage"/> class.
        /// </summary>
        /// <param name="nameValueCollection">Collection of key value pairs.</param>
        public OpenIdDynamicMessage(NameValueCollection nameValueCollection)
        {
            if (nameValueCollection == null)
                throw LogHelper.LogArgumentNullException("nameValueCollection");

            foreach (var key in nameValueCollection.AllKeys)
            {
                if (key != null)
                {
                    SetParameter(key, nameValueCollection[key]);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenIdDynamicMessage"/> class.
        /// </summary>
        /// <param name="parameters">Enumeration of key value pairs.</param>        
        public OpenIdDynamicMessage(IEnumerable<KeyValuePair<string, string[]>> parameters)
        {
            if (parameters == null)
                throw LogHelper.LogArgumentNullException("parameters");

            foreach (KeyValuePair<string, string[]> keyValue in parameters)
            {
                if (keyValue.Value != null && !string.IsNullOrWhiteSpace(keyValue.Key))
                {
                    foreach (string strValue in keyValue.Value)
                    {
                        if (strValue != null)
                        {
                            SetParameter(keyValue.Key, strValue);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenIdDynamicMessage"/> class.
        /// </summary>
        /// <param name="json">the json object from which the instance is created.</param>
        public OpenIdDynamicMessage(JObject json)
        {
            SetJsonParameters(json);
        }

        private void SetJsonParameters(JObject json)
        {
            if (json == null)
                throw LogHelper.LogArgumentNullException("json");

            foreach (var pair in json)
            {
                JToken value;
                if (json.TryGetValue(pair.Key, out value))
                {
                    SetParameter(pair.Key, value.ToString());
                }
            }
        }

        /// <summary>
        /// Returns a new instance of <see cref="OpenIdDynamicMessage"/> with values copied from this object.
        /// </summary>
        /// <returns>A new <see cref="OpenIdDynamicMessage"/> object copied from this object</returns>
        /// <remarks>This is a shallow Clone.</remarks>
        public virtual OpenIdDynamicMessage Clone()
        {
            return new OpenIdDynamicMessage(this);
        }

        /// <summary>
        /// Creates an OpenIdConnect message using the current contents of this <see cref="OpenIdDynamicMessage"/>.
        /// </summary>
        /// <returns>The uri to use for a redirect.</returns>
        public virtual string CreateAuthenticationRequestUrl()
        {
            OpenIdDynamicMessage openIdMessage = Clone();
            openIdMessage.RequestType = OpenIdDynamicRequestType.Authentication;
            EnsureTelemetryValues(openIdMessage);
            return openIdMessage.BuildRedirectUrl();
        }

        /// <summary>
        /// Creates a query string using the using the current contents of this <see cref="OpenIdDynamicMessage"/>.
        /// </summary>
        /// <returns>The uri to use for a redirect.</returns>
        public virtual string CreateLogoutRequestUrl()
        {
            OpenIdDynamicMessage openIdConnectMessage = Clone();
            openIdConnectMessage.RequestType = OpenIdDynamicRequestType.Logout;
            EnsureTelemetryValues(openIdConnectMessage);
            return openIdConnectMessage.BuildRedirectUrl();
        }

        /// <summary>
        /// Adds telemetry values to the message parameters.
        /// </summary>
        private void EnsureTelemetryValues(OpenIdDynamicMessage clonedMessage)
        {
            if (this.EnableTelemetryParameters)
            {
                clonedMessage.SetParameter(OpenIdParameterNames.SkuTelemetry, _skuTelemetryValue);
                clonedMessage.SetParameter(OpenIdParameterNames.VersionTelemetry, typeof(OpenIdDynamicMessage).GetTypeInfo().Assembly.GetName().Version.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the value for the AuthorizationEndpoint
        /// </summary>
        public string AuthorizationEndpoint { get; set; }

        /// <summary>
        /// Gets or sets 'access_Token'.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707", Justification = "Follows protocol names")]
        public string AccessToken
        {
            get { return GetParameter(OpenIdParameterNames.AccessToken); }
            set { SetParameter(OpenIdParameterNames.AccessToken, value); }
        }

        /// <summary>
        /// Gets or sets 'acr_values'.
        /// </summary>
        public string AcrValues 
        {
            get { return GetParameter(OpenIdParameterNames.AcrValues); }
            set { SetParameter(OpenIdParameterNames.AcrValues, value); }
        }

        /// <summary>
        /// Gets or sets 'claims_Locales'.
        /// </summary>
        public string ClaimsLocales
        {
            get { return GetParameter(OpenIdParameterNames.ClaimsLocales); }
            set { SetParameter(OpenIdParameterNames.ClaimsLocales, value); }
        }

        /// <summary>
        /// Gets or sets 'client_assertion'.
        /// </summary>
        public string ClientAssertion
        {
            get { return GetParameter(OpenIdParameterNames.ClientAssertion); }
            set { SetParameter(OpenIdParameterNames.ClientAssertion, value); }
        }

        /// <summary>
        /// Gets or sets 'client_assertion_type'.
        /// </summary>
        public string ClientAssertionType
        {
            get { return GetParameter(OpenIdParameterNames.ClientAssertionType); }
            set { SetParameter(OpenIdParameterNames.ClientAssertionType, value); }
        }

        /// <summary>
        /// Gets or sets 'client_id'.
        /// </summary>
        public string ClientId
        {
            get { return GetParameter(OpenIdParameterNames.ClientId); }
            set { SetParameter(OpenIdParameterNames.ClientId, value); }
        }

        /// <summary>
        ///  Gets or sets 'client_name'.
        /// </summary>
        public string ClientName
        {
            get { return GetParameter(OpenIdParameterNames.ClientName);  }
            set { SetParameter(OpenIdParameterNames.ClientName, value);  }
        }

        /// <summary>
        /// Gets or sets 'client_secret'.
        /// </summary>
        public string ClientSecret
        {
            get { return GetParameter(OpenIdParameterNames.ClientSecret); }
            set { SetParameter(OpenIdParameterNames.ClientSecret, value); }
        }

        /// <summary>
        /// Gets or sets 'code'.
        /// </summary>
        public string Code
        {
            get { return GetParameter(OpenIdParameterNames.Code); }
            set { SetParameter(OpenIdParameterNames.Code, value); }
        }

        /// <summary>
        /// Gets or sets 'display'.
        /// </summary>
        public string Display
        {
            get { return GetParameter(OpenIdParameterNames.Display); }
            set { SetParameter(OpenIdParameterNames.Display, value); }
        }

        /// <summary>
        /// Gets or sets 'domain_hint'.
        /// </summary>
        public string DomainHint
        {
            get { return GetParameter(OpenIdParameterNames.DomainHint); }
            set { SetParameter(OpenIdParameterNames.DomainHint, value); }
        }

        /// <summary>
        /// Gets or sets whether parameters for the library and version are sent on the query string for this <see cref="OpenIdDynamicMessage"/> instance. 
        /// This value is set to the value of EnableTelemetryParametersByDefault at message creation time.
        /// </summary>
        public bool EnableTelemetryParameters { get; set; } = EnableTelemetryParametersByDefault;


        /// <summary>
        /// Gets or sets whether parameters for the library and version are sent on the query string for all instances of <see cref="OpenIdDynamicMessage"/>. 
        /// </summary>
        public static bool EnableTelemetryParametersByDefault { get; set; } = true;

        /// <summary>
        /// Gets or sets 'error'.
        /// </summary>
        public string Error
        {
            get { return GetParameter(OpenIdParameterNames.Error); }
            set { SetParameter(OpenIdParameterNames.Error, value); }
        }

        /// <summary>
        /// Gets or sets 'error_description'.
        /// </summary>
        public string ErrorDescription
        {
            get { return GetParameter(OpenIdParameterNames.ErrorDescription); }
            set { SetParameter(OpenIdParameterNames.ErrorDescription, value); }
        }

        /// <summary>
        /// Gets or sets 'error_uri'.
        /// </summary>
        public string ErrorUri
        {
            get { return GetParameter(OpenIdParameterNames.ErrorUri); }
            set { SetParameter(OpenIdParameterNames.ErrorUri, value); }
        }

        /// <summary>
        /// Gets or sets 'expires_in'.
        /// </summary>
        public string ExpiresIn
        {
            get { return GetParameter(OpenIdParameterNames.ExpiresIn); }
            set { SetParameter(OpenIdParameterNames.ExpiresIn, value); }
        }

        /// <summary>
        /// Gets or sets 'grant_type'.
        /// </summary>
        public string GrantType
        {
            get { return GetParameter(OpenIdParameterNames.GrantType); }
            set { SetParameter(OpenIdParameterNames.GrantType, value); }
        }

        /// <summary>
        /// Gets or sets 'id_token'.
        /// </summary>
        public string IdToken
        {
            get { return GetParameter(OpenIdParameterNames.IdToken); }
            set { SetParameter(OpenIdParameterNames.IdToken, value); }
        }

        /// <summary>
        /// Gets or sets 'id_token_hint'.
        /// </summary>
        public string IdTokenHint
        {
            get { return GetParameter(OpenIdParameterNames.IdTokenHint); }
            set { SetParameter(OpenIdParameterNames.IdTokenHint, value); }
        }

        /// <summary>
        /// Gets or sets 'identity_provider'.
        /// </summary>
        public string IdentityProvider
        {
            get { return GetParameter(OpenIdParameterNames.IdentityProvider); }
            set { SetParameter(OpenIdParameterNames.IdentityProvider, value); }
        }

        /// <summary>
        /// Gets or sets 'iss'.
        /// </summary>
        public string Iss
        {
            get { return GetParameter(OpenIdParameterNames.Iss); }
            set { SetParameter(OpenIdParameterNames.Iss, value); }
        }

        /// <summary>
        /// Gets or sets 'login_hint'.
        /// </summary>
        [property: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707")]  
        public string LoginHint
        {
            get { return GetParameter(OpenIdParameterNames.LoginHint); }
            set { SetParameter(OpenIdParameterNames.LoginHint, value); }
        }

        /// <summary>
        /// Gets or sets 'max_age'.
        /// </summary>
        public string MaxAge
        {
            get { return GetParameter(OpenIdParameterNames.MaxAge); }
            set { SetParameter(OpenIdParameterNames.MaxAge, value); }
        }

        /// <summary>
        /// Gets or sets 'nonce'.
        /// </summary>
        public string Nonce
        {
            get { return GetParameter(OpenIdParameterNames.Nonce); }
            set { SetParameter(OpenIdParameterNames.Nonce, value); }
        }

        /// <summary>
        /// Gets or sets 'password'.
        /// </summary>
        public string Password
        {
            get { return GetParameter(OpenIdParameterNames.Password); }
            set { SetParameter(OpenIdParameterNames.Password, value); }
        }

        /// <summary>
        /// Gets or sets 'post_logout_redirect_uri'.
        /// </summary>
        public string PostLogoutRedirectUri
        {
            get { return GetParameter(OpenIdParameterNames.PostLogoutRedirectUri); }
            set { SetParameter(OpenIdParameterNames.PostLogoutRedirectUri, value); }
        }

        /// <summary>
        /// Gets or sets 'prompt'.
        /// </summary>
        public string Prompt
        {
            get { return GetParameter(OpenIdParameterNames.Prompt); }
            set { SetParameter(OpenIdParameterNames.Prompt, value); }
        }

        /// <summary>
        /// Gets or sets 'redirect_uri'.
        /// </summary>
        public string RedirectUri
        {
            get { return GetParameter(OpenIdParameterNames.RedirectUri); }
            set { SetParameter(OpenIdParameterNames.RedirectUri, value); }
        }
        /// <summary>
        /// Gets or sets a list of 'redirect_uris'.   TODO - this should be an array of strings
        /// </summary>
        public string RedirectUris
        {
            get { return GetParameter(OpenIdParameterNames.RedirectUris); }
            set { SetParameter(OpenIdParameterNames.RedirectUris, value); }
        }

        /// <summary>
        /// Gets or sets 'refresh_token'.
        /// </summary>
        public string RefreshToken
        {
            get { return GetParameter(OpenIdParameterNames.RefreshToken); }
            set { SetParameter(OpenIdParameterNames.RefreshToken, value); }
        }

        /// <summary>
        /// Gets or set the request type for this message
        /// </summary>
        /// <remarks>This is helpful when sending different messages through a common routine, when extra parameters need to be set or checked.</remarks>
        public OpenIdDynamicRequestType RequestType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets 'request_uri'.
        /// </summary>
        public string RequestUri
        {
            get { return GetParameter(OpenIdParameterNames.RequestUri); }
            set { SetParameter(OpenIdParameterNames.RequestUri, value); }
        }

        /// <summary>
        /// Gets or sets 'response_mode'.
        /// </summary>
        public string ResponseMode
        {
            get { return GetParameter(OpenIdParameterNames.ResponseMode); }
            set { SetParameter(OpenIdParameterNames.ResponseMode, value); }
        }

        /// <summary>
        /// Gets or sets 'response_type'.
        /// </summary>
        public string ResponseType
        {
            get { return GetParameter(OpenIdParameterNames.ResponseType); }
            set { SetParameter(OpenIdParameterNames.ResponseType, value); }
        }
        /// <summary>
        /// Gets or sets 'response_types'.
        /// </summary>
        public string ResponseTypes
        {
            get { return GetParameter(OpenIdParameterNames.ResponseTypes); }
            set { SetParameter(OpenIdParameterNames.ResponseTypes, value); }
        }
        /// <summary>
        /// Gets or sets 'resource'
        /// </summary>
        public string Resource
        {
            get { return GetParameter(OpenIdParameterNames.Resource); }
            set { SetParameter(OpenIdParameterNames.Resource, value); }
        }
        
        /// <summary>
        /// Gets or sets 'scope'.
        /// </summary>
        public string Scope
        {
            get { return GetParameter(OpenIdParameterNames.Scope); }
            set { SetParameter(OpenIdParameterNames.Scope, value); }
        }

        /// <summary>
        /// Gets or sets 'session_state'.
        /// </summary>
        public string SessionState
        {
            get { return GetParameter(OpenIdParameterNames.SessionState); }
            set { SetParameter(OpenIdParameterNames.SessionState, value); }
        }

        /// <summary>
        /// Gets or sets 'sid'.
        /// </summary>
        public string Sid
        {
            get { return GetParameter(OpenIdParameterNames.Sid); }
            set { SetParameter(OpenIdParameterNames.Sid, value); }
        }

        /// <summary>
        /// Gets or sets 'state'.
        /// </summary>
        public string State
        {
            get { return GetParameter(OpenIdParameterNames.State); }
            set { SetParameter(OpenIdParameterNames.State, value); }
        }

        /// <summary>
        /// Gets or sets 'target_link_uri'.
        /// </summary>
        public string TargetLinkUri
        {
            get { return GetParameter(OpenIdParameterNames.TargetLinkUri); }
            set { SetParameter(OpenIdParameterNames.TargetLinkUri, value); }
        }

        /// <summary>
        /// Gets or sets the value for the token endpoint.
        /// </summary>
        public string TokenEndpoint { get; set; }

        /// <summary>
        /// Gets or sets 'token_type'.
        /// </summary>
        public string TokenType
        {
            get { return GetParameter(OpenIdParameterNames.TokenType); }
            set { SetParameter(OpenIdParameterNames.TokenType, value); }
        }

        /// <summary>
        /// Gets or sets 'ui_locales'.
        /// </summary>
        public string UiLocales
        {
            get { return GetParameter(OpenIdParameterNames.UiLocales); }
            set { SetParameter(OpenIdParameterNames.UiLocales, value); }
        }

        /// <summary>
        /// Gets or sets 'user_id'.
        /// </summary>
        public string UserId
        {
            get { return GetParameter(OpenIdParameterNames.UserId); }
            set { SetParameter(OpenIdParameterNames.UserId, value); }
        }
        
        /// <summary>
        /// Gets or sets 'username'.
        /// </summary>
        public string Username
        {
            get { return GetParameter(OpenIdParameterNames.Username); }
            set { SetParameter(OpenIdParameterNames.Username, value); }
        }
    }
}