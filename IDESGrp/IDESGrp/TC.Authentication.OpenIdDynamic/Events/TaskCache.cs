﻿//TaskCache.cs

using System.Threading.Tasks;

namespace Microsoft.Extensions.Internal
{
    internal static class TaskCache
    {
        public static readonly Task CompletedTask = Task.CompletedTask;
    }
}
