// UserInformationReceivedContext.cs

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace TC.Authentication.OpenIdDynamic
{
    public class UserInformationReceivedContext : BaseOpenIdDynamicContext
    {
        public UserInformationReceivedContext(HttpContext context, OpenIdDynamicOptions options)
            : base(context, options)
        {
        }

        public JObject User { get; set; }
    }
}
