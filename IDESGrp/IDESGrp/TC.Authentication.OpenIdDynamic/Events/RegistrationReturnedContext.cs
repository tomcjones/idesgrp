// Copyright (c) Tom Jones - derived from .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;

namespace TC.Authentication.OpenIdDynamic
{
    /// <summary>
    /// This Context can be used to be informed when an 'Registration' is received over the OpenIdConnect protocol.
    /// </summary>
    public class RegistrationReturnedContext : BaseControlContext
    {
        /// <summary>
        /// Creates a <see cref="RegistrationReturnedContext"/>
        /// </summary>
        public RegistrationReturnedContext(HttpContext context, OpenIdDynamicOptions options)
            : base(context)
        {
            var foo = context.Request;
        }

        public OpenIdDynamicMessage ProtocolMessage { get; set; }

        public AuthenticationProperties Properties { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="JwtSecurityToken"/> that was received in the ation response, if any.
        /// </summary>
        public JwtSecurityToken JwtSecurityToken { get; set; }

        /// <summary>
        /// The configured communication channel to the identity provider for use when making custom requests to the token endpoint.
        /// </summary>
        public HttpClient Backchannel { get; internal set; }

        /// <summary>
        /// If the developer chooses to redeem the code themselves then they can provide the resulting tokens here. This is the
        /// same as calling HandleCodeRedemption. If set then the middleware will not attempt to redeem the code. An IdToken
        /// is required if one had not been previously received in the authorization response. An access token is optional
        /// if the middleware is to contact the user-info endpoint.
        /// </summary>
        public OpenIdDynamicMessage RegistrationResponse { get; set; }

        /// <summary>
        /// Indicates if the developer choose to handle (or skip) the code redemption. If true then the middleware will not attempt
        /// to redeem the code. See HandleCodeRedemption and TokenEndpointResponse.
        /// </summary>
        public bool HandledCodeRedemption => RegistrationResponse != null;

        /// <summary>
        /// Tells the middleware to skip the code redemption process. The developer may have redeemed the code themselves, or
        /// decided that the redemption was not required. If tokens were retrieved that are needed for further processing then
        /// call one of the overloads that allows providing tokens. An IdToken is required if one had not been previously received
        /// in the authorization response. An access token can optionally be provided for the middleware to contact the
        /// user-info endpoint. Calling this is the same as setting TokenEndpointResponse.
        /// </summary>
        public void HandleCodeRedemption()
        {
            RegistrationResponse = new OpenIdDynamicMessage();
        }

        /// <summary>
        /// Tells the middleware to skip the code redemption process. The developer may have redeemed the code themselves, or
        /// decided that the redemption was not required. If tokens were retrieved that are needed for further processing then
        /// call one of the overloads that allows providing tokens. An IdToken is required if one had not been previously received
        /// in the authorization response. An access token can optionally be provided for the middleware to contact the
        /// user-info endpoint. Calling this is the same as setting TokenEndpointResponse.
        /// </summary>
        public void HandleCodeRedemption(string accessToken, string idToken)
        {
            RegistrationResponse = new OpenIdDynamicMessage() { AccessToken = accessToken, IdToken = idToken };
        }

        /// <summary>
        /// Tells the middleware to skip the code redemption process. The developer may have redeemed the code themselves, or
        /// decided that the redemption was not required. If tokens were retrieved that are needed for further processing then
        /// call one of the overloads that allows providing tokens. An IdToken is required if one had not been previously received
        /// in the authorization response. An access token can optionally be provided for the middleware to contact the
        /// user-info endpoint. Calling this is the same as setting TokenEndpointResponse.
        /// </summary>
        public void HandleCodeRedemption(OpenIdDynamicMessage tokenEndpointResponse)
        {
            RegistrationResponse = tokenEndpointResponse;
        }
    }
}