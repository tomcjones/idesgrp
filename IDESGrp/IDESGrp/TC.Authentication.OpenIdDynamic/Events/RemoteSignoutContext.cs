// RemoteSignoutContext.cs

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace TC.Authentication.OpenIdDynamic
{
    public class RemoteSignOutContext : BaseOpenIdDynamicContext
    {
        public RemoteSignOutContext(
            HttpContext context,
            OpenIdDynamicOptions options,
            OpenIdConnectMessage message)
            : base(context, options)
        {
            ProtocolMessage = message;
        }
    }
}