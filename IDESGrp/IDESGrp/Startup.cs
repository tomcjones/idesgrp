﻿// Startup.cs for IDESGrp web application using external authencation 

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using IDESGrp.Data;
using IDESGrp.Models;
using IDESGrp.Services;

namespace IDESGrp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();                       // adds cookie-based authN to pipeline

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            // Before any secrets are used, enable client secrets as described in the link at "add user secrets above".
            //
            string CId = Configuration["TCGoogleClientID"];
            string CSec = Configuration["TCGoogleSecret"];
            GoogleOptions gOptions = new GoogleOptions()
            {
                ClientId = CId,
                ClientSecret = CSec
            };
            gOptions.Scope.Add("email");   //added at suggestion on http://stackoverflow.com/questions/19775321/owins-getexternallogininfoasync-always-returns-null/29921451#29921451 
                                           //this may not be needed when user record already exists, but when it doesn't 
                                           //then it could be used to populate the user and email names
            if (!string.IsNullOrWhiteSpace (CId) && !string.IsNullOrWhiteSpace(CSec ))
            {
                app.UseGoogleAuthentication(gOptions);
            }
            else
            {
                // TODO Call application insights in this case
            }

            string[] lsAdmins = null;
            try
            {
                string sAdmins = Configuration["BaseAdministrators:admin"];  // TODO  not working
                lsAdmins = sAdmins.Split(' ');  // get the list of users who are system admins
            }
            catch { }   // no problem if BaseAdministrators is not needed

            // OpenID Connect using the local Dynamic code
            OpenIdDynamicOptions oOptions = new OpenIdDynamicOptions()
            {
                ClientId = "IDESGrp",                 // this is a default value that will be changed during any client registrations
                BasicAuthMethodRequied = true,        // only for testing
                RequireHttpsMetadata = true,          // false only during early development
                GetClaimsFromUserInfoEndpoint = true, // since aspnetcore will not accept url framents, this seems like the only option
                BaseAdministrators = lsAdmins
            };
            //           oOptions.ClaimActions.Add("profile", "profile");
            app.UseOpenIdDynamicAuthentication(oOptions);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
