﻿// ApplicatonUser.cs copyright tom jones

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace IDESGrp.Models
{
    // Additonal profile attribute data for application user object
    public class ApplicationUser : IdentityUser
    {
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public int MemberType { get; set; }    // eg, 0 = untyped, 1 = identity proofed, 2 = Member in good standing, 3 = Suspended, 4 = Resigned
        public int EmailType { get; set; }     // eg, 0 = untyped, 1 = Email prohibited, 2 = Email accepted, 3 = Email digest only
        public string TimeZone { get; set; }   // can be offset from GMT or three character code that permits Daylight Savings use
    }
}
