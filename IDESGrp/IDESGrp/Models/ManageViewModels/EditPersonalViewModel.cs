﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IDESGrp.Models.ManageViewModels
{
    public class EditPersonalViewModel
    {
        [Required]
        [StringLength(80, ErrorMessage = "The {0} must be at least {2} and at most {1} characters long.", MinimumLength = 4)]
        [DataType(DataType.Text)]
        [Display(Name = "Unique User Name")]
        public string UserName { get; set; }

        [StringLength(254, ErrorMessage = "The {0} must be less than {1} characters long.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [StringLength(254, ErrorMessage = "The {0} must the less than {2} and at most {1} characters long.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Legal Family Name")]
        public string FamilyName { get; set; }

        [StringLength(254, ErrorMessage = "The {0} must the less than {2} and at most {1} characters long.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Legal Given Names")]
        public string GivenName { get; set; }

        [StringLength(254, ErrorMessage = "The {0} must the less than {2} and at most {1} characters long.", MinimumLength = 4)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        public string HomePhone { get; set; }

        [StringLength(254, ErrorMessage = "The {0} must the less than {2} and at most {1} characters long.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "TimeZone")]
        public string TimeZone{ get; set; }
    }
}