﻿// HomeController.cs

using Microsoft.AspNetCore.Mvc;

namespace IDESGrp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "IDESG RP application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "RP application contact page.";

            return View();
        }

        public IActionResult FAQ()
        {
            ViewData["Message"] = "Frequently Asked Questions.";
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
