# README #

The IDESGrp solution contains ASP.NET Core package to authenticate using external OpenID Connect with dynamic registration. As such it allows a web site to become IDESG registered and OpenID certified with little effort as well as support the limited numder of Open Identity Providers with dynamic registration that are available today.

### What is this repository for? ###

* Quick summary: The solution has a quick start relying party web site as well as TC.AUTHENTICATION code for dynamic registration. These are inlcluded in a single solution for running on Visual Studio 2017 with ASP.NET core 1.x code. It is loaded in a single solution so that debugging is simplified in the RTM of Visual Studio 2017.
* Version 1.0.0
* Certification : The TC.AUTHENTICATION code makes the process of acquiring an OpenID RP certfications for basic, configuration and dynamic possible, which is not the case with ASP.NET starndard libraries.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up : Just clone the repository using Visual Studio 2017 with a Git or BitBucket extension, open the IDESGrp soluiton, run "dotnet restore" in the package manager and build the solution.
If you get an error showing duplicates in the assembly info, it might be neccessary to delete the duplates in those two files.
* Configuration of External Providers : For Google or other IDentity Providers with static registration
follow the instructions for ASP.NET Core as there is no change there. Support for Dynamic registration is through OpenID Dynamic. Changes to StartUp.cs are all that is needed to enable any authenticaiton provider. 
* Dependencies : ASP.NET Core 1.1 or later
* Database configuration : defaults to SQL, but any will work
* How to run tests
* Deployment instructions for Azure can just use the publish options for the project.
For the portal.azure.com use this path to set the external provider secrets (client_id and client-secret).
Browse > All resources > MyApp > Settings > Application settings.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner TomCJones will respond to issues posted on this site
